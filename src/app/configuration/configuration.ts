import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
  private apiLink = {
    main: 'http://apiwebvenues.wisepass.co/'
    // main: 'http://localhost:63002/'
  }

  private listAPI = [
    { name: 'validateemail', link: 'portaladmin/auth/validateemail' },
    { name: 'validateotp', link: 'portaladmin/auth/validateotp' },
    { name: 'getvenueinformation', link: 'portaladmin/venue/information' },
    { name: 'savevenueinformation', link: 'portaladmin/venue/v2/information' },

    { name: 'getallcompanies', link: 'portaladmin/venue/allcompanies' },
    { name: 'getadminpermissionreport', link: 'portaladmin/venue/adminpermission' },
    { name: 'updatmemberbybusiness', link: 'portaladmin/venue/updatmemberbybusiness' },
    { name: 'getVenue', link: 'portaladminnode/venue/v2/all' },
    { name: 'updateVenue', link: 'portaladminnode/venue/v2/update' },

    // Mobile
    { name: 'getChallenge', link: 'portal/getchallenge' },
    { name: 'challengeInsertUpdate', link: 'portal/challengeinsertupdate' },
    { name: 'getChallengeById', link: 'portal/getchallengebyid' },
    { name: 'getChallengeConditionType', link: 'portal/getChallengeConditionType' },
    { name: 'challengeConditionTypeInsertUpdate', link: 'portal/challengeconditiontypeinsertupdate' },
    { name: 'getPromocode', link: 'portal/getpromocode' },

    /********************************* AMIN START************************************/
    // REPORT
    { name: 'category', link: 'portalAdmin/categoryconsumptionreport' },
    { name: 'passactivity', link: 'portalAdmin/passactivityconsumptionreport' },
    { name: 'supplier', link: 'portalAdmin/supplierconsumptionreport' },
    { name: 'managementvenues', link: 'portalAdmin/managementvenues' }, // Only Singha
    { name: 'managementcities', link: 'portalAdmin/managementcitiesreport' },
    { name: 'bankvenues', link: 'portalAdmin/bankvenuereport' },
    /********************************* AMIN END************************************/

    { name: 'promocodeConsumptionReport', link: 'portal/user/promocodeconsumptionreport' },
    { name: 'userofpackage', link: 'portalAdmin/userofpackage' },

    // vendor payment
    { name: 'getvendorpayment', link: 'portalAdmin/vendorpayment' },
    { name: 'insertvendorpayment', link: 'portalAdmin/vendorpaymentinsertupdate' },
    { name: 'vendorpaymentupdate', link: 'portalAdmin/vendorpaymentupdate' },
    { name: 'reconciliationbyperiod', link: 'portalAdmin/reconciliationbyperiod' },
    { name: 'reconciliation', link: 'portalAdmin/reconciliation' },

    // push notification
    { name: 'pushNotification', link: 'portaladminnode/pushnotification/v2/all' },
    { name: 'getPushNotification', link: 'portalAdmin/pushnotification' },

    // Event
    { name: 'event', link: 'portalAdmin/events' },

    // Brand Product
    { name: 'insertbrandproduct', link: 'portalAdmin/insertbrandproduct' },
    { name: 'getbrandproduct', link: 'portalAdmin/getbrandproduct' },
    { name: 'fancountfb', link: 'portalAdmin/fancountfb' },

    // Appasflyer 
    { name: 'appasflyer', link: 'portalAdmin/appasflyer' }, 
    { name: 'getappasflyer', link: 'portalAdmin/getappasflyer' }, 

    // Dashboard
    { name: 'dashboard', link: 'portalAdmin/dashboard' }, 
  ];

  constructor() {
  }

  getAPILink(key) {
    var item = this.listAPI.filter(function (item) {
      return item.name === key;
    })[0];

    return this.apiLink.main + item.link;
  }
}
