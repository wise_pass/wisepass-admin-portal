import { Injectable } from '@angular/core';
import 'rxjs/add/observable/of';
import { Router } from '@angular/router';

@Injectable()
export class CheckTokenService {
    constructor(
        private _router: Router
      ) { }

    isCheck() {
        try {
            var token = localStorage.getItem('token');
            
            if (token)
            {
                return true;
            } else {
                this._router.navigate(['/logout']);
                return false;
            }
            // return jwt_decode(token);
        }
        catch (Error){
            this._router.navigate(['/logout']);
            return false;
        }
      }
}
