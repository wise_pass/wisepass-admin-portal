import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import * as jwt_decode from "jwt-decode";
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;
  infoJwt: JwtReturn;

  userMenu = [
    { title: 'Home', link: '/dashboard' },
    { title: 'Log out', link: '/logout' }
  ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private analyticsService: AnalyticsService,
              private layoutService: LayoutService,
              private router: Router) {
  }

  getDecodedAccessToken(token: string): any {
    try {
        return jwt_decode(token);
    }
    catch (Error){
        return null;
    }
  }

  ngOnInit() {
    this.infoJwt = this.getDecodedAccessToken(localStorage.getItem('token') ? localStorage.getItem('token') : '');
    if (this.infoJwt)
    {
      this.user = {
        name: this.infoJwt.email, picture: this.infoJwt.avatar
      };
    } else {
      this.router.navigate(['/logout']);
    }

    // this.user = {
    //   name: 'lam@wisepass.co', picture: 'https://previews.123rf.com/images/rikkyal/rikkyal1712/rikkyal171200010/90908344-bearded-man-s-face-hipster-character-fashion-silhouette-avata.jpg'
    // };
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
}

interface JwtReturn
{
  company_expired_date: string,
  company_id: string,
  company_name: string,
  connection_string: string,
  email: string,
  exp: number,
  iss: string,
  jti: string,
  private_api: string,
  private_menu: string,
  public_api: string,
  public_menu: string,
  refresh_token: string,
  sub: string,
  avatar: string,
}