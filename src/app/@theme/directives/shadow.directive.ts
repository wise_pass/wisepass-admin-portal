// import { Directive, ElementRef, Renderer2, OnInit, Input } from '@angular/core';

// @Directive({ selector: '[infShadow]' })
// export class InfShadowDirective implements OnInit {

//     @Input() infShadow: string;
//     @Input() infShadowX: string;
//     @Input() infShadowY: string;
//     @Input() infShadowBlur: string;

//     // constructor(elem: ElementRef, renderer: Renderer2) {
//     //    renderer.setStyle(elem.nativeElement, 'box-shadow', '2px 2px 12px #58A362');
//     // }

//     constructor(private el: ElementRef, private renderer: Renderer2) { }
//     ngOnInit() {
//         let shadowTmp = `${this.infShadowX} ${this.infShadowY} ${this.infShadowBlur} ${this.infShadow}`;
//         this.renderer.setStyle(this.el.nativeElement, 'box-shadow', shadowTmp);
//     }
// }