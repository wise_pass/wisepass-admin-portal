/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// import { JwtHelperService } from '@auth0/angular-jwt';
// import { JwtHttpInterceptor } from './services/http-auth-interceptor';
// import { AuthService } from './services/auth.service';
// import { TokenInterceptor } from './services/token.interceptor';
// import { JwtInterceptor } from '@auth0/angular-jwt/src/jwt.interceptor';
// import { httpInterceptorProviders } from './http-interceptors

import { StorageService } from './configuration/storage.service';
import { ConfigService } from './configuration/configuration';
import { LoginService } from './customize/login/login.service';

import { CustomizeLoginComponent } from './customize/login/login.component';
import { CustomizeLogoutComponent } from './customize/logout/logout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NbDatepickerModule } from '../../node_modules/@nebular/theme';



@NgModule({
  declarations: [
    AppComponent,
    CustomizeLoginComponent,
    CustomizeLogoutComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
    NbDatepickerModule.forRoot(),

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
  ],
  bootstrap: [AppComponent],
  providers: [
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TokenInterceptor,
    //   multi: true
    // },

    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: JwtInterceptor,
    //   multi: true
    // },

    StorageService,
    ConfigService,
    LoginService,
    { provide: APP_BASE_HREF, useValue: '/' },
    // httpInterceptorProviders
  ],
})
export class AppModule {
}
