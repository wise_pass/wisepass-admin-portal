import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { PromocodeConsumptionReportComponent } from './report/promocode-consumption-report/promocode-consumption-report.component';
import { ReconciliationComponent } from './report/reconciliationByVenue/reconciliation-by-venue.component';
import { PassActivityComponent } from './report/pass-activity/pass-activity.component';
import { CategoryComponent } from './report/category/category.component';
import { SupplierComponent } from './report/supplier/supplier.component';
import { ManagementVenuesComponent } from './report/ManagementVenues/management-venues.component';
import { ManagementCitiesComponent } from './report/ManagementCities/management-cities.component';
import { BankVenueComponent } from './report/BankVenue/bank-venue.component';
import { AppasflyerComponent } from './report/Appasflyer/appasflyer.component';
import { NbDialogModule } from '../../../../node_modules/@nebular/theme';
import { AppasflyerDetailComponent } from './report/Appasflyer/appasflyer-detail/appasflyer-detail.component';
// import { ShowcaseDialogComponent } from './modal-overlays/dialog/showcase-dialog/showcase-dialog.component';

export const ReportsRoutedComponents = [
  // Root 
  ReportsComponent,
  CategoryComponent,
  PassActivityComponent,
  SupplierComponent,
  PromocodeConsumptionReportComponent,
  ReconciliationComponent,
  ManagementVenuesComponent,
  ManagementCitiesComponent,
  BankVenueComponent,
  AppasflyerComponent,
  AppasflyerDetailComponent,
  // ShowcaseDialogComponent,
];


const routes: Routes = [{
  path: '',
  component: ReportsComponent,
  children: [
    {
      path: 'category',
      component: CategoryComponent,
    },
    {
      path: 'passactivity',
      component: PassActivityComponent,
    },
    {
      path: 'supplier',
      component: SupplierComponent,
    },
    {
      path: 'promocodeConsumptionReport',
      component: PromocodeConsumptionReportComponent,
    },
    {
      path: 'reconciliationbyperiod',
      component: ReconciliationComponent,
    },
    {
      path: 'managementvenues',
      component: ManagementVenuesComponent,
    },
    {
      path: 'managementcities',
      component: ManagementCitiesComponent,
    },
    {
      path: 'bankvenues',
      component: BankVenueComponent,
    },
    {
      path: 'appasflyer',
      component: AppasflyerComponent,
    },
    {
      path: '',
      redirectTo: 'category',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NbDialogModule.forChild(),
  ],
  exports: [
    RouterModule,
  ],
  entryComponents: [
    AppasflyerDetailComponent,
  ],
})
export class ReportsRoutingModule {

}

