import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { ReportsRoutedComponents, ReportsRoutingModule } from './reports-routing.module';
import { PromocodeConsumptionReportService } from './report/promocode-consumption-report/promocode-consumption-report.service';
import { ReconciliationByVenueService } from './report/reconciliationByVenue/reconciliation-by-venue.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbDatepickerModule, NbCalendarModule } from '../../../../node_modules/@nebular/theme';
import { PassActivityService } from './report/pass-activity/pass-activity.service';
import { CategoryService } from './report/category/category.service';
import { SupplierService } from './report/supplier/supplier.service';
import { ManagementVenuesService } from './report/ManagementVenues/management-venues.service';
import { ManagementCitiesService } from './report/ManagementCities/management-cities.service';
import { BankVenueService } from './report/BankVenue/bank-venue.service';
import { AppasflyerService } from './report/Appasflyer/appasflyer.service';
import { NgxEchartsModule } from '../../../../node_modules/ngx-echarts';

@NgModule({
  imports: [
    ThemeModule,
    ReportsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbDatepickerModule,
    NbCalendarModule,
    NgxEchartsModule,
  ],
  declarations: [
    ...ReportsRoutedComponents,
  ],
  providers: [
    CategoryService,
    PassActivityService,
    SupplierService,
    PromocodeConsumptionReportService,
    ReconciliationByVenueService,
    ManagementVenuesService,
    ManagementCitiesService,
    BankVenueService,
    AppasflyerService,
  ]
})
export class ReportsModule { }
