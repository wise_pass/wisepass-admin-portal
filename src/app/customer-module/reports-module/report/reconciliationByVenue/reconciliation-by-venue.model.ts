
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface ReconciliationByVenueList {
    reconciliationByVenueList: Array<ReconciliationByVenueModel>
}

export interface ReconciliationByVenueModel {
    businessVenueId: string;
    businessVenueName: string;
    addressName: string;
    businessContactEmail: string;
    price: number;
    totalPass: number;
    businessLogoImageUrl: string;
    reconciliationByProductTypeList: ReconciliationByProductTypeModel[];
}

export interface ReconciliationByProductTypeModel {
    productTypeId: string;
    productTypeName: string;
    totalPassProductType: number;
    totalPriceProductType: number;
    priceProductType: number;
    currencyIdentifier: string;
}
