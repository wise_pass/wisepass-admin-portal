import { Component, OnInit, OnChanges } from '@angular/core';
import { ReconciliationByVenueService } from './reconciliation-by-venue.service';
import { ResponseData, ReconciliationByVenueModel, ReconciliationByVenueList } from './reconciliation-by-venue.model';
import { NbToastrService } from '../../../../../../node_modules/@nebular/theme/components/toastr/toastr.service';
declare var $: any;

@Component({
  selector: 'ngx-reconciliation-by-venue',
  styleUrls: ['./reconciliation-by-venue.component.scss'],
  templateUrl: './reconciliation-by-venue.component.html',
})
export class ReconciliationComponent implements OnInit, OnChanges {

  loading: boolean;
  reconciliationByVenues: ReconciliationByVenueModel[];
  month: string;


  constructor(
    private _reconciliationByVenueService: ReconciliationByVenueService,
    private toastrService: NbToastrService,
  ) { }

  ngOnInit() {
    var now = new Date();
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];

    this.month = monthNames[now.getMonth() - 1] + '/' + now.getFullYear();

    $('#month').val(this.month);

    this.onVenueMonthSubmit();

    $('.datepicker').datepicker({
      format: 'MM/yyyy',
      viewMode: 'months',
      minViewMode: 'months'
    });

  }

  ngOnChanges() {
  }

  onVenueMonthSubmit() {
    const month = $('#month').val();
    const startMonth = (new Date(month)).toLocaleString();
    const currentmonth = new Date(month);
    const endMonth = new Date(currentmonth.getFullYear(), currentmonth.getMonth() + 1, 0, 23, 59, 59, 999);
    console.log();
    
    const body: any = {
      "firstDay": startMonth,
      "lastDay": endMonth.toLocaleString(),
    }

    this.loading = true;
    this._reconciliationByVenueService.getReconciliationByVenue(body)
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<ReconciliationByVenueList>;
            this.reconciliationByVenues = response.data.reconciliationByVenueList;
          } catch (error) {
            console.log('errors', error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }

  onSendReconciliationSubmit(){
    this._reconciliationByVenueService.sendReconciliation()
    .subscribe(
      data => {
        try {
          const response = data as ResponseData<ReconciliationByVenueList>;
          console.log(response);
          if (response.statusCode == 200) {
            this.toastrService.success('Success', this.month, { duration: 3000 });
          }
        } catch (error) {
          console.log('errors', error);
          // this._router.navigate(['/logout']);
        }
      }, error => {
        console.log('oops', error)
      },
      () => {
        // 'onCompleted' callback.
        // No errors, route to new page here
        this.loading = false;
      },
  );
  }
}

