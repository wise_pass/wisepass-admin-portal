import { Component, OnInit } from '@angular/core';
import { ResponseData, CategoryConsumptionByPeriodList, CategoryConsumptionByProductTypeList } from './category.model';
import { CategoryService } from './category.service';
declare var Chart: any;
declare var $: any;

@Component({
  selector: 'ngx-category',
  styleUrls: ['./category.component.scss'],
  templateUrl: './category.component.html',
})
export class CategoryComponent implements OnInit {

  lastmonth: string;
  currentmonth: string;
  listPassByPeriod: CategoryConsumptionByPeriodList[] = [];
  datePickerConfig = {
    format: 'MM-YYYY',
  };

  myChart;
  label = [];
  datasets = [];
  productTypeNames: CategoryConsumptionByProductTypeList[] = [];

  constructor(
    private _categoryService: CategoryService,
  ) { }

  ngOnInit() {
    var now = new Date();
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];

    var dateMonth = new Date(now.getFullYear(), now.getMonth() - 5, 1, 0, 0, 0);
    this.lastmonth = monthNames[dateMonth.getMonth()] + '/' + dateMonth.getFullYear();
    this.currentmonth = monthNames[now.getMonth()] + '/' + now.getFullYear();

    $('#lastMonth').val(this.lastmonth);
    $('#currentMonth').val(this.currentmonth);

    this.onSubmitPeriod();


    $('.datepicker').datepicker({
      format: 'MM/yyyy',
      viewMode: 'months',
      minViewMode: 'months'
    });

  }

  onDrawingBarPassByCategogy() {
    const ctx = document.getElementById('myChart');
    if (this.myChart && this.myChart.destroy) {
      this.myChart.destroy();
    }

    this.myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.label || [],
        datasets: this.datasets,
      },
      options: {
        scales: {
          xAxes: [{
            stacked: true,
            gridLines: { color: "#312164" },
            ticks: {
              fontColor: "#fff",
            },
          }],
          yAxes: [{
            stacked: true,
            gridLines: { color: "#312164" },
            ticks: {
              fontColor: "#fff",
            },
          }]
        }, // scales
        labels: {
          fontColor: '#ffffff'
        },
        tooltips: {
          mode: 'index',
          titleFontSize: 20,
          bodyFontSize: 16,
          callbacks: {
            label: function (tooltipItem, data) {
              var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || 'Other';
              // var label = data.labels[tooltipItem.index];
              const _value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              const _valuePercent = data.datasets[tooltipItem.datasetIndex].dataPercent[tooltipItem.index];
              return datasetLabel + ': ' + _value + ` (${_valuePercent}%)`;
            }
          }
        }, // tooltips
        legend: {
          labels: {
            fontColor: '#ffffff'
          }
        },
        multiTooltipTemplate: (e, i) => { console.log(e, i) },
      }, // option
    });
  }

  onSubmitPeriod() {
    const startDate = $('#lastMonth').val();
    const endDate = $('#currentMonth').val();

    const currentmonth = new Date(endDate);
    const lastDay = new Date(currentmonth.getFullYear(), currentmonth.getMonth() + 1, 0, 23, 59, 59, 999);

    const body: any = {
      'lastmonth': new Date(startDate).toLocaleString(),
      'currentmonth': lastDay.toLocaleString(),
    }
    this._categoryService.getCategory(body)
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<CategoryConsumptionByPeriodList>;
            this.listPassByPeriod = response.data.categoryConsumptionByPeriodList;
            // console.log('Call API Get Pass By Category', this.listPassByPeriod);

            const productTypes = []; 
            this.label = [];
            this.listPassByPeriod.forEach(passItem => {
              this.label.push(passItem.period);
              passItem.categoryConsumptionByProductTypeList.forEach(productTypeItem => {
                if (productTypes.findIndex(x => x.productTypeName == productTypeItem.productTypeName) == -1) {
                  const obj = {
                    productTypeId: productTypeItem.productTypeId,
                    productTypeName: productTypeItem.productTypeName,
                    totalScans: []
                  };
                  productTypes.push(obj);
                }
              });
            });

            this.listPassByPeriod.forEach(periodItem => {
              productTypes.forEach(ptItem => {
                const index = periodItem.categoryConsumptionByProductTypeList.findIndex(x => x.productTypeName == ptItem.productTypeName);
                if (index > -1) {
                  // exied
                  ptItem.totalScans.push(periodItem.categoryConsumptionByProductTypeList[index].totalScan);
                } else {
                  ptItem.totalScans.push(0);
                }
              });
            });

            // percent
            productTypes.forEach(productTypeItem => {
              productTypeItem.totalScanPercents = [];
              productTypeItem.totalScans.forEach((scanItem, index) => {
                let total = 0;
                productTypes.forEach(x => {
                  total += x.totalScans[index];
                });
                const percentage = ((scanItem / total) * 100).toFixed(1);
                productTypeItem.totalScanPercents.push(percentage);
              });
            });

            this.datasets = [];
            const color = ['#020065', '#8D94B6', '#FBCEB1', '#29AB87', '#FFE5B4', '#FA8072', '#D2B48C',
              '#7B3F00', '#00FF7F'];
            productTypes.forEach((productTypeItem, i) => {
              const obj = {
                label: productTypeItem.productTypeName,
                data: productTypeItem.totalScans,
                dataPercent: productTypeItem.totalScanPercents,

                backgroundColor: ((productTypeItem, index) => {
                  if (productTypeItem.productTypeName == 'Beer') {
                    return '#E7BA57';
                  } else if (productTypeItem.productTypeName == 'CGV') {
                    return '#EE0000';
                  } else if (productTypeItem.productTypeName == 'Starbucks') {
                    return '#376751';
                  } else if (productTypeItem.productTypeName == 'Cosmetics') {
                    return '#ad59ff';
                  } {
                    return color[index];
                  }
                })(productTypeItem, i),
                borderWidth: 1
              };
              this.datasets.push(obj);
            });

            console.log(productTypes);

            this.onDrawingBarPassByCategogy();
          } catch (error) {
            console.log('errors', error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }
}

