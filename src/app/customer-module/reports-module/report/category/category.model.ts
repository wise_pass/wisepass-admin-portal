
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface CategoryConsumptionByPeriodList {
    categoryConsumptionByPeriodList: Array<CategoryConsumptionByPeriodList>
}

export interface CategoryConsumptionByPeriodList {
    period: string;
    categoryConsumptionByProductTypeList: CategoryConsumptionByProductTypeList[];
}

export interface CategoryConsumptionByProductTypeList {
    productTypeId: string;
    productTypeName: string;
    totalScan: number;
    period: string;
}