


import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'ngx-appasflyer-detail',
    styleUrls: ['./appasflyer-detail.component.scss'],
    templateUrl: './appasflyer-detail.component.html',
})
export class AppasflyerDetailComponent implements OnInit {
    @Input() appasflyer;

    constructor(public activeModal: NgbActiveModal) { }

    ngOnInit() {
        console.log(this.appasflyer);
    }

}

