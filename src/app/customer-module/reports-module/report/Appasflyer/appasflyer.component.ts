import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../../node_modules/@angular/router';
import { AppasflyerService } from './appasflyer.service';
import { ResponseData, AppasflyerModel, AppasflyerInstallsModel } from './appasflyer.model';
import { NgbModal } from '../../../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { AppasflyerDetailComponent } from './appasflyer-detail/appasflyer-detail.component';
declare var $: any;

@Component({
  selector: 'ngx-appasflyer',
  styleUrls: ['./appasflyer.component.scss'],
  templateUrl: './appasflyer.component.html',
})
export class AppasflyerComponent implements OnInit {

  loading: boolean;
  htmlStr: string;
  options: any;
  echartsIntance: any;

  lastmonth: string;
  currentmonth: string;

  listSelectAppasflyer: AppasflyerInstallsModel[] = [];

  colors = [
    {
      lineColor1: '#7659ff',
      lineColor2: '#8357ff',
      areaColor1: 'rgba(78, 64, 164, 1)',
      areaColor2: 'rgba(118, 73, 208, 0.7)',
    },
    {
      lineColor1: '#00bece',
      lineColor2: '#00da78',
      areaColor1: 'rgba(38, 139, 145, 0.8)',
      areaColor2: 'rgba(38, 139, 145, 0.5)',
    },
    {
      lineColor1: '#42AAFF',
      lineColor2: '#42AAFF',
      areaColor1: '#42AAFF',
      areaColor2: '#42AAFF',
    }
  ];

  constructor(
    private _appasflyerService: AppasflyerService,
    private _router: Router,
    private modalService: NgbModal
  ) { }

  settings = {
    mode: 'external',
    columns: {
      createdDateYear: {
        title: 'Year',
        editable: false,
      },
      createdDateMonth: {
        title: 'Month',
        editable: false,
      },
      totalInstalls: {
        title: 'Installs',
        editable: false,
      },
      totalRegistrations: {
        title: 'Registrations',
        editable: false,
      },
      totalConversionRate1: {
        title: 'Conversion Rate',
        editable: false,
        valuePrepareFunction: (value) => {
          return value + '%';
        },
      },

    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    pager: {
      display: true,
      perPage: 20
    },

  };

  ngOnInit() {
    var now = new Date();
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];

    var dateMonth = new Date(now.getFullYear(), now.getMonth() - 5, 1, 0, 0, 0);
    this.lastmonth = monthNames[dateMonth.getMonth()] + '/' + dateMonth.getFullYear();
    this.currentmonth = monthNames[now.getMonth()] + '/' + now.getFullYear();

    $('#lastMonth').val(this.lastmonth);
    $('#currentMonth').val(this.currentmonth);

    this.onCallAPIAppasflyer();
    this.getOptions();

    $('.datepicker').datepicker({
      format: 'MM/yyyy',
      viewMode: 'months',
      minViewMode: 'months'
    });
  }


  onCallAPIAppasflyer() {

    const startDate = $('#lastMonth').val();
    const endDate = $('#currentMonth').val();

    const currentmonth = new Date(endDate);
    const lastDay = new Date(currentmonth.getFullYear(), currentmonth.getMonth() + 1, 0, 23, 59, 59, 999);

    const body: any = {
      'lastmonth': new Date(startDate).toLocaleString(),
      'currentmonth': lastDay.toLocaleString(),
    }

    this.loading = true;
    this._appasflyerService.getAppasflyer(body)
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<AppasflyerModel>;
            this.listSelectAppasflyer = response.data.appasflyerInstallsList;
            // console.log('Call API Get Appasflyer', this.listSelectAppasflyer);
            if (this.listSelectAppasflyer && this.listSelectAppasflyer.length > 0) {
              this.onDrawingChartInstallRegistrations(this.listSelectAppasflyer);
            }

          } catch (error) {
            console.log('errors', error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );

  }

  onAppasflyerDetailModalOpen(event) {
    const modalRef = this.modalService.open(AppasflyerDetailComponent);
    modalRef.componentInstance.appasflyer = event.data;
  }

  getOptions() {
    this.options = {
      "backgroundColor": "#3d3780",
      "tooltip": {
        "trigger": "axis",
        "axisPointer": {
          "type": "shadow",
          "shadowStyle": {
            "color": "rgba(0, 0, 0, 0.3)"
          }
        },
        textStyle: {
          color: "#ffffff",
          fontSize: "20",
          fontWeight: "normal"
        },
        position: "top",
        backgroundColor: "rgba(0, 255, 170, 0.35)",
        borderColor: "#00d977",
        borderWidth: 3,
        extraCssText: "box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 8px 24px;"
      },
      grid: {
        left: "1%",
        right: "1%",
        containLabel: true
      },
      "legend": {
        x: 'left',
        y: 'top',
        "data": ['Install', 'Registrations', 'ConversionRate'],
        textStyle: {
          color: '#fff'
        }
      },
      "xAxis": [
        {
          "type": "category",
          "data": [],
          "axisTick": {
            "alignWithLabel": true
          },
          "axisLine": {
            "lineStyle": {
              "color": "#a1a1e5"
            }
          },
          "axisLabel": {
            "color": "#a1a1e5",
            "fontSize": "14"
          }
        }
      ],
      yAxis: [
        {
          "type": "value",
          "boundaryGap": false,
          "axisLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.3)",
              "width": "1"
            }
          },
          "axisLabel": {
            "color": "#fff",
            "fontSize": "14"
          },
          "axisTick": {
            "show": false
          },
          "splitLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.2)",
              "width": "1"
            }
          }
        },
        {
          "type": "value",
          "boundaryGap": false,
          "axisLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.3)",
              "width": "1"
            }
          },
          labelStyle: {
            color: 'red'
          },
          "axisLabel": {
            "color": "#fff",
            "fontSize": "16",
            formatter: '{value}%'
          },
          "axisTick": {
            "show": false
          },
          "splitLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.2)",
              "width": "1"
            }
          }
        },
      ],
      "series": []
    }
  }

  onDrawingChartInstallRegistrations(listSelectAppasflyer: AppasflyerInstallsModel[]) {
    let arrXaxisPass = [];
    let arrInstall = [];
    let arrRegistrations = [];
    let arrConversionRate1 = [];

    listSelectAppasflyer.forEach(item => {
      arrXaxisPass.push(item.createdDateMonth);
      arrInstall.push(item.totalInstalls);
      arrRegistrations.push(item.totalRegistrations);
      arrConversionRate1.push(item.totalConversionRate1);
    });

    this.options.xAxis[0].data = arrXaxisPass;
    this.options.series = [];

    const series1 = this.getSeriesPass(this.colors[0].lineColor1, this.colors[0].lineColor2, 'Install', arrInstall, 'bar', 0);
    this.options.series.push(series1);

    const series2 = this.getSeriesPass(this.colors[1].lineColor1, this.colors[1].lineColor2, 'Registrations', arrRegistrations, 'bar', 0);
    this.options.series.push(series2);

    const series3 = this.getSeriesPass(this.colors[2].lineColor1, this.colors[2].lineColor2, 'ConversionRate', arrConversionRate1, 'line', 1);
    this.options.series.push(series3);

    this.echartsIntance.setOption(this.options);
  }

  private getSeriesPass(colorStops1, colorStops2, name, data, type, yAxisIndex) {
    return {
      "name": name,
      "type": type,
      "barGap": 0,
      "barWidth": "20%",
      yAxisIndex: yAxisIndex,
      smooth: true,
      "itemStyle": {
        "normal": {
          "color": {
            "x": 0,
            "y": 0,
            "x2": 0,
            "y2": 1,
            "type": "linear",
            "global": false,
            "colorStops": [
              {
                "offset": 0,
                "color": colorStops1
              },
              {
                "offset": 1,
                "color": colorStops2
              }
            ]
          }
        }
      },
      "data": data
    }
  }

  onChartInit(echarts) {
    this.echartsIntance = echarts;
  }
}


