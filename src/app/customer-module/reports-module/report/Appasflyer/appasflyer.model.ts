
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface AppasflyerModel {
    totalInstalls: number;
    appasflyerInstallsList: Array<AppasflyerInstallsModel>
}
export interface AppasflyerInstallsModel {
    id: string;
    createdDateYear: number;
    createdDateMonth: string;
    totalInstalls: number;
    totalConversionRate1: string;
    totalRegistrations: number;
    appasflyerInstallsDetailList : Array<AppasflyerInstallsDetailModel>
}

export interface AppasflyerInstallsDetailModel {
    id: string;
    installs: number;
    device: string;
    conversionRate1: number;
    registrations: number;
} 