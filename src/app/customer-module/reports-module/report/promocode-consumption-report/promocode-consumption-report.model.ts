
export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: Promo<T>;
}

export interface Promo<T> {
    promoCodes: Array<T>;
}

export interface PromoCodeModel {
    date: string;
    promoCodeType: PromoCodeType[];
}

export interface PromoCodeType {
    promoCodeTypeId: string;
    promoCodeTypeName: string;
    startMonth: string;
    endMonth: string;
    total: number;
}
