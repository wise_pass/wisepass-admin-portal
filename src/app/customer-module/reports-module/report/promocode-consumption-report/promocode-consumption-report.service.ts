import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import * as jwt_decode from 'jwt-decode';
import { ConfigService } from '../../../../configuration/configuration';

@Injectable()
export class PromocodeConsumptionReportService {
    constructor(private http: HttpClient, public _configService: ConfigService) {}

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error) {
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            },
        );
    }

    promocodeConsumptionReport(startMonth, endMonth) {
        this.initHttpHeader();
        var period = {
            "startMonth" : startMonth,
            "endMonth" : endMonth,
        }
        return this.http.post(
            this._configService.getAPILink('promocodeConsumptionReport').toString(),
            period,
            this.httpOptions);
    }
}
