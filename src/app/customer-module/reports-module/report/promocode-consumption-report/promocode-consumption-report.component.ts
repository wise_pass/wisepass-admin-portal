import { PromocodeConsumptionReportService } from "./promocode-consumption-report.service";
import { PromoCodeType, PromoCodeModel, ReponseDataList } from "./promocode-consumption-report.model";
import { OnInit, OnChanges, Component } from "../../../../../../node_modules/@angular/core";
declare var $: any;

@Component({
  selector: 'ngx-consumption-promocode-report',
  styleUrls: ['./promocode-consumption-report.scss'],
  templateUrl: './promocode-consumption-report.html',
})
export class PromocodeConsumptionReportComponent implements OnInit, OnChanges {

  startMonth: string;
  endMonth: string;
  promoCodes: PromoCodeModel[] = [];
  date: string;
  period: Date = new Date();
  days = [];
  reports = [];
  promCodeList: PromoCodeType[] = [];
  date2 = ('0' + (new Date().getMonth() + 1)).slice(-2) + '-' + new Date().getFullYear();

  constructor(
    private _consumptionPromocodeReportService: PromocodeConsumptionReportService,
  ) { }

  ngOnInit() {
    this.onAddChallenge();
    $('.datepicker').datepicker({
      format: 'MM/yyyy',
      viewMode: 'months',
      minViewMode: 'months'
    });
  }

  ngOnChanges() {
  }

  getDaysInMonth(month, year) {
    const days = [];
    var date = new Date(Date.UTC(year, month, 1));
    while (date.getMonth() === month) {
      days.push(date.getDate());
      date.setDate(date.getDate() + 1);
    }
    return days;
  }

  onAddChallenge() {
    const arr = this.date2.split('-');
    let monthCurrent = new Date(parseInt(arr[1], 10), parseInt(arr[0], 10) - 1, 1);
    this.startMonth = new Date(new Date(monthCurrent.getFullYear(), monthCurrent.getMonth(), 1, 0, 0, 0)).toLocaleDateString();
    this.endMonth = new Date(new Date(monthCurrent.getFullYear(), monthCurrent.getMonth() + 1, 0)).toLocaleDateString();
    this.days = this.getDaysInMonth(monthCurrent.getMonth(), monthCurrent.getFullYear());

    this._consumptionPromocodeReportService.promocodeConsumptionReport(this.startMonth, this.endMonth)
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<PromoCodeModel>;
            this.promoCodes = response.data.promoCodes;
            // console.log('Call API Get User Promocode Consumption Report', this.promoCodes);

            // get list promo code type 
            this.promCodeList = [];
            this.promoCodes.forEach(element => {
              // this.date =  element.date;
              element.promoCodeType.forEach(promoCodeTypeItem => {
                if (this.promCodeList.findIndex(x => x.promoCodeTypeId === promoCodeTypeItem.promoCodeTypeId) == -1) {
                  const temp = Object.assign({}, promoCodeTypeItem);
                  temp.total = null;
                  this.promCodeList.push(temp);
                }
              });
            });

            // report {day, promoCodeType: kieu promocode type []}
            this.reports = [];
            this.days.forEach(day => {
              const promoCode = this.promoCodes.find(x => x.date == day);
              if (promoCode) {
                // have day found
                const reportItem = {
                  day: day,
                  promoCodeType: [],
                };
                this.promCodeList.forEach(promCodeItem => {
                  const promoCodeTypeExisted = promoCode.promoCodeType.find(x => x.promoCodeTypeId == promCodeItem.promoCodeTypeId);
                  if (promoCodeTypeExisted) {
                    reportItem.promoCodeType.push(promoCodeTypeExisted);
                  } else {
                    reportItem.promoCodeType.push(promCodeItem);
                  }
                });
                this.reports.push(reportItem);
              } else {
                // not found => total = 0
                const reportItem = {
                  day: day,
                  promoCodeType: [],
                };
                this.promCodeList.forEach(promCodeItem => {
                  reportItem.promoCodeType.push(promCodeItem);
                });
                this.reports.push(reportItem);
              }
            });
          } catch (error) {
            console.log("errors", error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }


}

