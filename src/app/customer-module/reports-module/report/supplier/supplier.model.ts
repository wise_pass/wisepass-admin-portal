
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface BusinessConsumptionList {
    businessConsumptionList: Array<BusinessConsumptionModel>
}

export interface BusinessConsumptionModel {
    businessId: string;
    businessName: string;
    total: number;
    businessVenueMonthConsumptionList: BusinessVenueMonthConsumptionModel[];
}

export interface BusinessVenueMonthConsumptionModel {
    month: string;
    year: number;
    businessVenueId: string;
    businessVenueName: string;
    businessVenueTypeName: string;
    fullAddress: string;
    city: string;
    totalPass: number;
    totalUnique: number;
}