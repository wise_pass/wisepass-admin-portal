import { Component, OnInit, OnChanges } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { SupplierService } from './supplier.service';
import { ResponseData, BusinessConsumptionList, BusinessConsumptionModel, BusinessVenueMonthConsumptionModel } from './supplier.model';

@Component({
  selector: 'ngx-supplier',
  styleUrls: ['./supplier.component.scss'],
  templateUrl: './supplier.component.html',
})
export class SupplierComponent implements OnInit, OnChanges {

  userId: string;
  listmonthConsumption: BusinessConsumptionModel[];
  listBusinessConsumption = [];
  listVenueConsumption: BusinessVenueMonthConsumptionModel[];
  venues = [];
  dateArray = [];
  businesss = [];
  isValid: boolean = true;
  htmlStr: string;
  selectedItemBusiness: string = "Pernod Ricard Vietnam";
  loading: boolean;

  settings = {
    columns: {
      businessVenueName: {
        title: 'Venue'
      },
      businessVenueTypeName: {
        title: 'Category', width: '15%',
      },
      city: {
        title: 'City', width: '15%',
      },
      totalPass: {
        title: 'PASS', width: '10%',
      },
      totalMembership: {
        title: 'Consumers', width: '10%',
      },
      month: {
        title: 'Month', sort: false, width: '10%',
      },
      year: {
        title: 'Year', sort: false, width: '10%',
      },
    },
    actions: false,
    pager: {
      display: true,
      perPage: 20
    },
  };

  constructor(
    private _supplierService: SupplierService,
  ) { }

  ngOnInit() {
    this.onVenueMonthSubmit();
  }

  ngOnChanges() {
    this.onBusinessChanged();
  }

  onVenueMonthSubmit() {
    try {
      const infoJwt = jwt_decode(localStorage.getItem('token') ? localStorage.getItem('token') : '');
      if (infoJwt) {
        this.userId = infoJwt.UserId;
      }
    } catch (e) {

    }

    this.loading = true;
    this._supplierService.getSupplierConsumption()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<BusinessConsumptionList>;
            this.listmonthConsumption = response.data.businessConsumptionList;
            // console.log('Call API Get Venue Month Consumpiton', this.listmonthConsumption);
            if (this.listmonthConsumption && this.listmonthConsumption.length > 0) {
              this.selectedItemBusiness = this.listmonthConsumption[0].businessId;
              this.onBusinessChanged();
              this.htmlStr = '';
            } else {
              this.htmlStr = '<h3 class="text-center">Not data</h3>';
            }
          } catch (error) {
            console.log('errors', error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }

  onBusinessChanged() {
    if (this.listmonthConsumption) {
      const business = this.listmonthConsumption.find(x => x.businessId === this.selectedItemBusiness);
      this.listVenueConsumption = business.businessVenueMonthConsumptionList;
    }
  }

}

