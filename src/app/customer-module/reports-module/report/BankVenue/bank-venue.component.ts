import {OnInit, Component } from '@angular/core';
import { Router } from '../../../../../../node_modules/@angular/router';
import { BankVenueService } from './bank-venue.service';
import { ResponseData, BankVenueModel, BankVenueDetailModel } from './bank-venue.model';


@Component({
  selector: 'ngx-bank-venue',
  styleUrls: ['./bank-venue.component.scss'],
  templateUrl: './bank-venue.component.html',
})
export class BankVenueComponent implements OnInit {

  loading: boolean;
  listBankVenues: BankVenueDetailModel[];

  constructor(
    private _bankVenueService: BankVenueService,
    private _router: Router,
  ) { }

  settings = {
    columns: {
      venueName: {
        title: 'Venue',
      },
      bankAccountNumber: {
        title: 'Bank Account Number',
      },
      bankName: {
        title: 'Bank Name',
      },
      companyName: {
        title: 'Company Name',
      },
    },
    actions: false,
    pager: {
      display: true,
      perPage: 20
    },
  };

  ngOnInit() {
    this.onAPIBankVenues();
  }

  onAPIBankVenues() {
    this.loading = true;
    this._bankVenueService.getBankVenues()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<BankVenueModel>;
            this.listBankVenues = response.data.bankVenueDetailList;
            console.log('Call API Get Management Venues', this.listBankVenues);
            
          } catch (error) {
            console.log('errors', error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }

}


