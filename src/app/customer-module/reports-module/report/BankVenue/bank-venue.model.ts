
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface BankVenueModel {
    bankVenueDetailList: Array<BankVenueDetailModel>
}

export interface BankVenueDetailModel {
    venueName: string;
    bankAccountNumber: string;
    bankName: string;
    companyName: string;
}