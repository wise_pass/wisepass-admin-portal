
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface PassActivityDetailList {
    passActivityDetailList: Array<PassActivityDetailList>
}

export interface PassActivityDetailList {
    period: string;
    productTypeId: string;
    productTypeName: string;
    totalScan: number;
    scanQuanlity: number;
    userQuanlity: number;
    totalAmount: number;
    averageCost: number;
}