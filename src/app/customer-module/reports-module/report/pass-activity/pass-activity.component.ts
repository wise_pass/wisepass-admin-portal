import { Component, OnInit } from '@angular/core';
import { ResponseData, PassActivityDetailList } from './pass-activity.model';
import { PassActivityService } from './pass-activity.service';
import { Router } from '../../../../../../node_modules/@angular/router';
declare var $: any;

@Component({
  selector: 'ngx-pass-activity',
  styleUrls: ['./pass-activity.component.scss'],
  templateUrl: './pass-activity.component.html',
})
export class PassActivityComponent implements OnInit {

  options: any;
  echartsIntance: any;
  loading: boolean;

  listPassActivity: PassActivityDetailList[];
  lastmonth: string;
  currentmonth: string;
  myChart;
  totalAmounts = [];
  labels = [];
  datasets = [];
  averageCost = [];

  constructor(
    private _passActivityService: PassActivityService,
    private _router: Router,
  ) { }


  colors = [
    {
      lineColor1: '#7659ff',
      lineColor2: '#8357ff',
      areaColor1: 'rgba(78, 64, 164, 1)',
      areaColor2: 'rgba(118, 73, 208, 0.7)',
    },
    {
      lineColor1: '#00bece',
      lineColor2: '#00da78',
      areaColor1: 'rgba(38, 139, 145, 0.8)',
      areaColor2: 'rgba(38, 139, 145, 0.5)',
    },
    {
      lineColor1: '#42AAFF',
      lineColor2: '#42AAFF',
      areaColor1: '#42AAFF',
      areaColor2: '#42AAFF',
    }
  ];


  settings = {
    columns: {
      period: {
        title: 'Period',
      },
      userQuanlity: {
        title: 'Consumers',
      },
      scanQuanlity: {
        title: 'PASS',
      },
      totalAmount: {
        title: 'Cost (VND)',
        valuePrepareFunction: (value) => value === 'Total' ? value : Intl.NumberFormat('vi-VN').format(value)
      },
      averageCost: {
        title: 'Cost per PASS (VND)',
        valuePrepareFunction: (value) => value === 'Total' ? value : Intl.NumberFormat('vi-VN').format(value)
      },
    },
    actions: false,
    pager: {
      display: true,
      perPage: 20
    },
  };

  ngOnInit() {
    var now = new Date();
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];

    var dateMonth = new Date(now.getFullYear(), now.getMonth() - 5, 1, 0, 0, 0);
    this.lastmonth = monthNames[dateMonth.getMonth()] + '/' + dateMonth.getFullYear();
    this.currentmonth = monthNames[now.getMonth()] + '/' + now.getFullYear();

    $('#lastMonth').val(this.lastmonth);
    $('#currentMonth').val(this.currentmonth);

    this.onSubmitPeriod();
    this.getOptions();


    $('.datepicker').datepicker({
      format: 'MM/yyyy',
      viewMode: 'months',
      minViewMode: 'months'
    });

  }

  getOptions() {
    this.options = {
      "backgroundColor": "#3d3780",
      "tooltip": {
        "trigger": "axis",
        "axisPointer": {
          "type": "shadow",
          "shadowStyle": {
            "color": "rgba(0, 0, 0, 0.3)"
          }
        },
        textStyle: {
          color: "#ffffff",
          fontSize: "20",
          fontWeight: "normal"
        },
        position: "top",
        backgroundColor: "rgba(0, 255, 170, 0.35)",
        borderColor: "#00d977",
        borderWidth: 3,
        extraCssText: "box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 8px 24px;"
      },
      grid: {
        left: "1%",
        right: "1%",
        containLabel: true
      },
      "legend": {
        x: 'left',
        y: 'top',
        "data": ['Cost', 'Cost Per'],
        textStyle: {
          color: '#fff'
        }
      },
      "xAxis": [
        {
          "type": "category",
          "data": [],
          "axisTick": {
            "alignWithLabel": true
          },
          "axisLine": {
            "lineStyle": {
              "color": "#a1a1e5"
            }
          },
          "axisLabel": {
            "color": "#a1a1e5",
            "fontSize": "14"
          }
        }
      ],
      yAxis: [
        {
          "type": "value",
          "boundaryGap": false,
          "axisLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.3)",
              "width": "1"
            }
          },
          "axisLabel": {
            "color": "#fff",
            "fontSize": "14"
          },
          "axisTick": {
            "show": false
          },
          "splitLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.2)",
              "width": "1"
            }
          }
        },
        {
          "type": "value",
          "boundaryGap": false,
          "axisLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.3)",
              "width": "1"
            }
          },
          labelStyle: {
            color: 'red'
          },
          "axisLabel": {
            "color": "#fff",
            "fontSize": "16",
          },
          "axisTick": {
            "show": false
          },
          "splitLine": {
            "lineStyle": {
              "color": "rgba(161, 161 ,229, 0.2)",
              "width": "1"
            }
          }
        },
      ],
      "series": []
    }
  }

  onSubmitPeriod() {
    this.loading = true;

    const startDate = $('#lastMonth').val();
    const endDate = $('#currentMonth').val();

    const currentmonth = new Date(endDate);
    const lastDay = new Date(currentmonth.getFullYear(), currentmonth.getMonth() + 1, 0, 23, 59, 59, 999);

    const body: any = {
      'lastmonth': new Date(startDate).toLocaleString(),
      'currentmonth': lastDay.toLocaleString(),
    }
    this._passActivityService.getPassActivity(body)
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<PassActivityDetailList>;
            this.listPassActivity = response.data.passActivityDetailList;
            console.log('Call API Get Pass By Activity', this.listPassActivity);

            this.labels = [];
            this.totalAmounts = [];
            this.datasets = [];
            this.averageCost = [];

            (this.listPassActivity || []).forEach(periodItem => {
              this.labels.push(periodItem.period);
              this.totalAmounts.push(periodItem.totalAmount);
              this.averageCost.push(periodItem.averageCost);
            });

            if (this.listPassActivity && this.listPassActivity.length > 0) {
              this.onDrawingBarLinePassByActivity(this.listPassActivity);
            }
          } catch (error) {
            console.log('errors', error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }


  onDrawingBarLinePassByActivity(listPassActivity: PassActivityDetailList[]) {
    let arrXaxisPass = [];
    let arrCost = [];
    let arrCostPer = [];

    listPassActivity.forEach(item => {
      arrXaxisPass.push(item.period);
      arrCost.push(item.totalAmount);
      arrCostPer.push(item.averageCost);
    });

    this.options.xAxis[0].data = arrXaxisPass;
    this.options.series = [];

    const series1 = this.getSeriesPass(this.colors[0].lineColor1, this.colors[0].lineColor2, 'Cost', arrCost, 'bar', 0);
    this.options.series.push(series1);

    const series2 = this.getSeriesPass(this.colors[2].lineColor1, this.colors[2].lineColor2, 'Cost Per', arrCostPer, 'line', 1);
    this.options.series.push(series2);

    this.echartsIntance.setOption(this.options);
  }

  private getSeriesPass(colorStops1, colorStops2, name, data, type, yAxisIndex) {
    return {
      "name": name,
      "type": type,
      "barGap": 0,
      "barWidth": "20%",
      yAxisIndex: yAxisIndex,
      smooth: true,
      "itemStyle": {
        "normal": {
          "color": {
            "x": 0,
            "y": 0,
            "x2": 0,
            "y2": 1,
            "type": "linear",
            "global": false,
            "colorStops": [
              {
                "offset": 0,
                "color": colorStops1
              },
              {
                "offset": 1,
                "color": colorStops2
              }
            ]
          }
        }
      },
      "data": data
    }
  }

  onChartInit(echarts) {
    this.echartsIntance = echarts;
  }


}

