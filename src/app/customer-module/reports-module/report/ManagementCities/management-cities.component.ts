import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../../node_modules/@angular/router';
import { ManagementCitiesService } from './management-cities.service';
import { ResponseData, ManagementCitiesByInternationalZoneModel, ManagementCitiesByVenueModel } from './management-cities.model';


@Component({
  selector: 'ngx-management-cities',
  styleUrls: ['./management-cities.component.scss'],
  templateUrl: './management-cities.component.html',
})
export class ManagementCitiesComponent implements OnInit {

  loading: boolean;
  listManagementCities: ManagementCitiesByInternationalZoneModel[];
  listManagementVenues: ManagementCitiesByVenueModel[];
  selectedItemCity: string = "Thailand";
  totalVenue: number = 0;

  constructor(
    private _managementCitiesService: ManagementCitiesService,
    private _router: Router,
  ) { }

  settings = {
    columns: { 
      businessVenueName: {
        title: 'Venue',
      },
      businessVenueTypeName: {
        title: 'Category',
        width: '30%',
      },
      cityLocationName: {
        title: 'City',
        width: '30%',
        sort: false,
      },
      isActive: {
        title: 'Status',
        type: 'html',
        width: '6%',
        valuePrepareFunction: (value) => {
          return value === true ? '<span class="btn-success">ON</span>' : '<span class="btn-danger">OFF</span>';
        }
      },
    },
    actions: false,
    pager: {
      display: true,
      perPage: 20
    },
  };

  ngOnInit() {
    this.onAPIManagementVenues();
  }

  onAPIManagementVenues() {
    this.loading = true;
    this._managementCitiesService.getManagementCities()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<ManagementCitiesByInternationalZoneModel>;
            this.listManagementCities = response.data.managementCitiesByInternationalZoneList;
            // console.log('Call API Get Management Venues', this.listManagementCities);
            if (this.listManagementCities && this.listManagementCities.length > 0) {
              this.selectedItemCity = this.listManagementCities[0].internationalZoneId;
              this.onCityChanged();
            }
          } catch (error) {
            console.log('errors', error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }

  onCityChanged() {
    if (this.listManagementCities) {
      const venue = this.listManagementCities.find(x => x.internationalZoneId === this.selectedItemCity);
      this.listManagementVenues = venue.managementCitiesByVenueList;
      console.log(this.listManagementVenues);
    }
  }

}


