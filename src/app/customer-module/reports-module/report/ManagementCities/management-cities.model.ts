
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface ManagementCitiesByInternationalZoneModel {
    managementCitiesByInternationalZoneList: Array<ManagementCitiesByInternationalZoneModel>
}

export interface ManagementCitiesByInternationalZoneModel {
    internationalZoneId: string;
    internationalZoneName: string;
    totalVenue: number;
    managementCitiesByVenueList: ManagementCitiesByVenueModel[];
}

export interface ManagementCitiesByVenueModel {
    businessVenueId: string;
    businessVenueName: string;
    businessVenueTypeName: string;
    venueIsActive: boolean;
    numericalOrder: number;
}