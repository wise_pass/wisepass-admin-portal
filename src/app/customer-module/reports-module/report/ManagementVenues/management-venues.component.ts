import { Component, OnInit } from '@angular/core';
import { ManagementVenuesService } from './management-venues.service';
import { ResponseData, ManagementVenuesModel, ManagementVenuesByBusinessModel, ManagementVenuesByVenueModel } from './management-venues.model';
import { error } from '../../../../../../node_modules/@angular/compiler/src/util';
import { Router } from '../../../../../../node_modules/@angular/router';


@Component({
  selector: 'ngx-management-venues',
  styleUrls: ['./management-venues.component.scss'],
  templateUrl: './management-venues.component.html',
})
export class ManagementVenuesComponent implements OnInit {

  loading: boolean;
  listManagementBusiness: ManagementVenuesByBusinessModel[];
  listManagementVenues: ManagementVenuesByVenueModel[];
  totalVenue = 0;
  totalVenueActive = 0;
  totalVenueDeactive = 0;

  constructor(
    private _managementVenuesService: ManagementVenuesService,
    private _router: Router,
  ) { }

  settings = {
    columns: {
      numericalOrder: {
        title: 'STT',
        width: '5%'
      },
      businessVenueName: {
        title: 'Venue'
      },
      businessVenueTypeName: {
        title: 'Category',
        width: '30%',
      },
      city: {
        title: 'City',
        width: '30%',
      },
      isActive: {
        title: 'Status',
        type: 'html',
        width: '6%',
        valuePrepareFunction: (value) => {
          return value === true ?  '<span class="btn-success">ON</span>' : '<span class="btn-danger">OFF</span>';
        },
        // filterFunction(cell?: any, search?: string): boolean {
        //   const match = cell.text.toLowerCase().indexOf(search) > -1;
        //   console.log('match',match,'cell',cell);
        //   if (match || search === '') {
        //     return true;
        //   } else {
        //     return false;
        //   }
        // },
      },
    },
    actions: false,
    pager: {
      display: true,
      perPage: 20
    },
  };

  ngOnInit() {
    this.onAPIManagementVenues();
  }

  onAPIManagementVenues() {
    this.loading = true;
    this._managementVenuesService.getManagementVenues()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<ManagementVenuesModel>;
            this.listManagementBusiness = response.data.managementVenuesByBusinessList;
            // console.log('Call API Get Management Venues', this.listManagementBusiness);
            let turnCountActive = 0;
            let turnCountDeactive = 0;
            if (this.listManagementBusiness && this.listManagementBusiness.length > 0) {
              this.listManagementBusiness.forEach(businessItem => {
                this.totalVenue = businessItem.totalVenue;
                this.listManagementVenues = businessItem.managementVenuesByVenueList;
                this.listManagementVenues.forEach(venueItem => {
                  if (venueItem.isActive === true) {
                    this.totalVenueActive = turnCountActive++;
                  } else if (venueItem.isActive === false) {
                    this.totalVenueDeactive = turnCountDeactive++;
                  } else {
                    console.log('errors', error);
                  }
                });
              });
            }
          } catch (error) {
            console.log('errors', error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }
}

