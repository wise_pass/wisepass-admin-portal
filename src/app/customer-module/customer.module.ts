import { NgModule } from '@angular/core';

import { CustomerComponent } from './customer.component';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { NbSelectModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { CustomerRoutingModule } from './customer-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const PAGES_COMPONENTS = [
  CustomerComponent,
];

@NgModule({
  imports: [
    CustomerRoutingModule,
    ThemeModule,
    MiscellaneousModule,
    NbSelectModule,
    FormsModule,
    Ng2SmartTableModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class CustomerModule {
}
