import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { ChallengeService } from './challenge/challenge.service';
import { ChallengesRoutingModule, challengesRoutedComponents } from './challenges-routing.module';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    ChallengesRoutingModule,
    FormsModule,                              
    ReactiveFormsModule 
  ],
  declarations: [
    ...challengesRoutedComponents,
  ],
  providers: [
    ChallengeService,
  ]
})
export class ChallengesModule { }
