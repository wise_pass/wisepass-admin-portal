import { Component, OnInit, OnChanges } from '@angular/core';
import { ChallengeService } from '../challenge/challenge.service';
import { ReponseDataListChallengeConditionType, ChallengeConditionTypeModel } from './challenge-condition-type.model';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-challenge-condition-type',
  styleUrls: ['./challenge-condition-type.component.scss'],
  templateUrl: './challenge-condition-type.component.html',
})
export class ChallengeConditionTypeComponent implements OnInit, OnChanges {

  listChallengeConditionType: ChallengeConditionTypeModel[] = [];

  constructor(
    private _challengeService: ChallengeService,
    private router: Router
  ) { }

  ngOnInit() {
    this.onChallengeConditionType();
  }

  ngOnChanges() {
  }

  onChallengeConditionType() {
    this._challengeService.getChallengeConditionType()
    .subscribe(
      data => {
        try {
          const response = data as ReponseDataListChallengeConditionType<ChallengeConditionTypeModel>;
          this.listChallengeConditionType = response.data;
          console.log('Call API Challenge Condition Type', response);

        } catch (error) {
          console.log("errors", error);
          // this._router.navigate(['/logout']);
        }
      }, error => {
        console.log('oops', error)
      },
      () => {
        // 'onCompleted' callback.
        // No errors, route to new page here
      },
  );
  }

  onAddChallengeConditionType() {
    this.router.navigateByUrl('customer/challenges/challengeConditionTypeInsertUpdate');
  };

  onEditChallengeConditionType(id){
    this.router.navigate(['customer/challenges/challengeConditionTypeInsertUpdate'], { queryParams: {id : id} });
  }
}

