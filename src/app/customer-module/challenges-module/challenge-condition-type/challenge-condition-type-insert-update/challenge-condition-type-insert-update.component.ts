import { Component, OnInit, OnChanges } from '@angular/core';
import { Router } from '../../../../../../node_modules/@angular/router';
import { NbToastrService } from '../../../../../../node_modules/@nebular/theme/components/toastr/toastr.service';
import { ChallengeService } from '../../challenge/challenge.service';
import { ChallengeConditionTypeModel, ReponseDataListChallengeConditionType } from '../challenge-condition-type.model';

@Component({
  selector: 'ngx-challenge-condition-type-insert-update',
  styleUrls: ['./challenge-condition-type-insert-update.component.scss'],
  templateUrl: './challenge-condition-type-insert-update.component.html',
})
export class UpdateChallengeConditionComponent implements OnInit, OnChanges {

  id: string;
  value: string;

  constructor(
    private _challengeService: ChallengeService,
    private toastrService: NbToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  
  }

  ngOnChanges() {
  }

  onAddChallengeConditionType() {
    if (!this.value || !this.value.trim()) {
      this.toastrService.warning('', 'Value required', { duration: 3000 });
      return;
    }

    this._challengeService.insertUpdateChallengeConditionType(this.value)
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataListChallengeConditionType<ChallengeConditionTypeModel>;
            console.log('Call API Isert/Update Challenge Condition Type', response);
            if (response.statusCode == 200) {
              this.toastrService.success('Success', this.value, { duration: 3000 });
              this.router.navigateByUrl('customer/challengs/getChallengeConditionType');
            }
          } catch (error) {
            console.log("errors", error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }

  onBackChallengeConditionType() {
    this.router.navigateByUrl('customer/challenges/getChallenge');
  }
}

