
export interface ReponseDataListChallengeConditionType<T> {
    message: string;
    statusCode: number;
    data: Array<T>;
}

export interface ChallengeConditionTypeModel {
    id: string,
    value: string
}
