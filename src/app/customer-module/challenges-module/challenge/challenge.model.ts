
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}

export interface ChallengeDetailList{
    challengeDetailList : Array<ChallengeDetailModel>
}

export interface ChallengeDetailModel {
    id: string,
    imageUrl: string,
    title: string,
    details: string,
    dateFrom: string,
    dateTo: string,
    createdDate: string,
    createdBy: string,
    isActive: boolean,
    isDelete: boolean,
    email: string,
    image: string,
}

export interface PromocodeTypeList{
    promocodeTypeList : Array<PromocodeTypeModel>
}

export interface PromocodeTypeModel {
    promocodeTypeId: string,
    promocodeTypeName: string,
    promocodeList: PromoCodeListModel[]
}

export interface PromoCodeListModel {
    promocodeId: string,
    promocodeUniqueString: string
}

// user package
export interface PackageList{
    packageList : Array<PackageListModel>
}

export interface PackageListModel {
    packageId: string,
    packageName: string
    membershipUserPackageList: MembershipUserPackageList[]
}

export interface MembershipUserPackageList {
    membershipUserId: string;
    memberShipUserEmail: string;
}