import { Component, OnInit, OnChanges } from '@angular/core';
import { PromocodeTypeModel, PackageListModel, ResponseData, ChallengeDetailList, ChallengeDetailModel, PromoCodeListModel, PromocodeTypeList, PackageList } from '../challenge.model';
import { ChallengeService } from '../challenge.service';
import * as jwt_decode from 'jwt-decode';
import { ActivatedRoute, Router } from '../../../../../../node_modules/@angular/router';
import { NbToastrService } from '../../../../../../node_modules/@nebular/theme/components/toastr/toastr.service';
import { FormGroup, FormBuilder } from '../../../../../../node_modules/@angular/forms';

@Component({
  selector: 'ngx-challenge-insert-update',
  styleUrls: ['./challengeInsertUpdate.component.scss'],
  templateUrl: './challengeInsertUpdate.component.html',
})
export class UpdateChallengeComponent implements OnInit, OnChanges {

  selectedOption: string = "1";
  listChallenge: ChallengeDetailModel[] = [];
  listPackage: PackageListModel[] = [];
  title: string;
  detail: string;
  image: any; // type = file
  createBy: string;
  dateFrom: string;
  dateTo: string; // yyyy-mm-dd
  id = '';
  base64textString: string[] = [];

  // condition
  titleCondition: string;
  detailCondition: string;
  createByCondition: string;
  dateFromCondition: string;
  dateToCondition: string;
  valueCondition: number;

  // select 
  selectChallengeConditionType: string;
  selectPromotionCodeType: string;
  selectPromotionCode: string;
  promoCodeTypeList: PromocodeTypeModel[] = [];
  promocodeList: PromoCodeListModel[] = [];

  selectedPackageItem: string;
  challengeConditionType =
    [
      {
        "id": "6B7D0CF2-E064-4090-9D7E-19DE01EA10B3",
        "value": "PASSCGV"
      },
      {
        "id": "40D1BF53-C289-4252-BB22-1B2EC6CDE3C3",
        "value": "PASSBEERSINGHA"
      },
      {
        "id": "FA919269-92C7-4633-88D6-4553D1F382E1",
        "value": "PASSSTARBUCK"
      },
    ];

  // membershipId
  email: string;

  // tab
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  fourForm: FormGroup;

  constructor(
    private _challengeService: ChallengeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: NbToastrService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    const infoJwt = this.getDecodedAccessToken(localStorage.getItem('token') ? localStorage.getItem('token') : '');
    if (infoJwt) {
      this.createBy = infoJwt.email
      this.createByCondition = infoJwt.email
    } else {
      this.toastrService.danger('Session Timeout');
    }

    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.getChallengeById(this.id);
      }
    });

    if (this.challengeConditionType.length > 0) {
      this.selectChallengeConditionType = this.challengeConditionType[0].id;
    }

    // API Promocode 
    this._challengeService.getPromocode()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<PromocodeTypeList>;
            // console.log("TCL: UpdateChallengeComponent -> ngOnInit -> this.promoCodeType", response)
            this.promoCodeTypeList = response.data.promocodeTypeList;
            if (this.promoCodeTypeList && this.promoCodeTypeList.length > 0) {
              this.selectPromotionCodeType = this.promoCodeTypeList[0].promocodeTypeId;
            }

            this.onChangeProCodeType(this.selectPromotionCodeType);

            if (this.promocodeList && this.promocodeList.length > 0) {
              this.selectPromotionCode = this.promocodeList[0].promocodeId;
            }

          } catch (error) {
            console.log("errors", error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );



    // tab
    this.firstForm = this.fb.group({
      // firstCtrl: ['', Validators.required],
    });

    this.secondForm = this.fb.group({
      // secondCtrl: ['', Validators.required],
    });

    this.thirdForm = this.fb.group({
      // thirdCtrl: ['', Validators.required],
    });
    this.fourForm = this.fb.group({
      // thirdCtrl: ['', Validators.required],
    });

    this.onChange(this.selectedOption);
  }

  // tab
  onFirstSubmit() {
    this.firstForm.markAsDirty();
  }

  onSecondSubmit() {
    this.secondForm.markAsDirty();
  }

  onThirdSubmit() {
    this.thirdForm.markAsDirty();
  }

  onFourSubmit() {
    this.fourForm.markAsDirty();
  }


  onChangeProCodeType(selectPromotionCodeType) {
    const promoCodeTypeItem = this.promoCodeTypeList.find(x => x.promocodeTypeId == selectPromotionCodeType);
    this.promocodeList = promoCodeTypeItem.promocodeList;
    if (this.promocodeList && this.promocodeList.length > 0) {
      this.selectPromotionCode = this.promocodeList[0].promocodeId;
    }
    this.onChangeProCode(this.selectPromotionCode);
  }

  onChangeProCode(selectPromotionCode) {
    // console.log("fe",selectPromotionCode);
  }
  onChangeChallengeConditionType(selectChallengeConditionType) {
    // console.log("TCL: UpdateChallengeComponent -> onChangeChallengeConditionType -> selectChallengeConditionType", selectChallengeConditionType)
  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  ngOnChanges() {

  }

  onUploadChange(evt: any) {
    const file = evt.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(e) {
    this.base64textString = ['data:image/png;base64,' + btoa(e.target.result)];
  }

  onSubmitChallenge() {
    if (!this.title || !this.title.trim()
      || !this.detail || !this.detail.trim()) {
      this.toastrService.warning('', 'Title and Detail are required', { duration: 3000 });
      return;
    }

    const body: any = {
      "title": this.title,
      "details": this.detail,
      "imageUrl": this.base64textString[0],
      "createdBy": this.createBy,
      "dateFrom": this.dateFrom,
      "dateTo": this.dateTo,

      "titleCondition": this.titleCondition,
      "detailsCondition": this.detailCondition,
      "createdByCondition": this.createByCondition,
      "dateFromCondition": this.dateFromCondition,
      "dateToCondition": this.dateToCondition,
      "valueCondition": this.valueCondition,
      "idConditionType": this.selectChallengeConditionType,

      // membershipUser
      "email": this.email,
      "promocodeId": this.selectPromotionCode
    };

    if (this.id) {
      body.id = this.id;
    }

    this._challengeService.insertUpdateChallenge(body)
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<ChallengeDetailList>;
            this.listChallenge = response.data.challengeDetailList;
            // console.log('Call API Isert/Update Challenge', response);
            if (response.statusCode == 200) {
              this.toastrService.success('Success', this.title, { duration: 3000 });
              this.router.navigateByUrl('customer/challenges/getChallenge');
            }
          } catch (error) {
            console.log("errors", error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }

  getChallengeById(id: string) {
    // this._challengeService.getChallengeById(id).subscribe((res: any) => {
    //   const challenge: ChallengeDetailModel = res.data;
    //   console.log("TCL: UpdateChallengeComponent -> getChallengeById -> challenge", challenge)

    //   this.title = challenge.title;
    //   this.detail = challenge.details;
    //   this.email = challenge.email;
    //   this.base64textString = [challenge.imageUrl];
    //   this.email = challenge.email;
    //   this.createBy = challenge.createdBy;
    //   this.dateFrom = this.formatDate(new Date(challenge.dateFrom));
    //   this.dateTo = this.formatDate(new Date(challenge.dateTo));
    //   this.valueCondition = challenge.valueCondition;
    //   this.titleCondition = challenge.titleCondition;
    //   this.detailCondition = challenge.detailCondition;
    //   this.dateFromCondition = this.formatDate(new Date(challenge.dateFromCondition));
    //   this.dateToCondition = this.formatDate(new Date(challenge.dateToCondition));
    // }, (err) => {
    //   console.log(err);
    // })
  }

  formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) { month = '0' + month };
    if (day.length < 2) { day = '0' + day };

    return [year, month, day].join('-');
  }

  onPackageChange(selectedPackageItem) {
    // console.log(selectedPackageItem);
  }

  onChange(selectedOption) {
    console.log(selectedOption);
    if (selectedOption == "1") {
      this.email = "All";
    } else
      if (selectedOption == "2") {
        this.email = "";
      } else {
        // API user package
        this._challengeService.getUserOfPackage()
          .subscribe(
            data => {
              try {
                const response = data as ResponseData<PackageList>;
                // console.log('Call API Isert/Update Package', response);
                this.listPackage = response.data.packageList;
                if (this.listPackage && this.listPackage.length > 0) {

                  this.email = this.listPackage[0].packageId
                }
              } catch (error) {
                console.log("errors", error);
                // this._router.navigate(['/logout']);
              }
            }, error => {
              console.log('oops', error)
            },
            () => {
              // 'onCompleted' callback.
              // No errors, route to new page here
            },
        );
      }
  }
}

