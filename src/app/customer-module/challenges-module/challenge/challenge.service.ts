import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/observable/of';
import * as jwt_decode from 'jwt-decode';
import { ConfigService } from '../../../configuration/configuration';

@Injectable()
export class ChallengeService {
    constructor(
        private httpClient: HttpClient,
        public _configService: ConfigService
    ) { }

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error) {
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            }
        );
    }

    getChallenge() {
        this.initHttpHeader();
        return this.httpClient.get(
            this._configService.getAPILink('getChallenge').toString(),
            this.httpOptions);
    }

    insertUpdateChallenge(body) {
        this.initHttpHeader();
        return this.httpClient.post(
            this._configService.getAPILink('challengeInsertUpdate').toString(),
            body, 
            this.httpOptions);
    }

    getChallengeById(id: string) {
        this.initHttpHeader();
        let params = new HttpParams();

        // Begin assigning parameters
        params = params.append('id', id);
        const headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            }
        );
        const options = {
            headers,
            params,
        }

        return this.httpClient.get(
            this._configService.getAPILink('getChallengeById').toString(),
            options);
    }


    // Challenge Condition Type
    getChallengeConditionType() {
        this.initHttpHeader();
        return this.httpClient.get(
            this._configService.getAPILink('getChallengeConditionType').toString(),
            this.httpOptions);
    }

    insertUpdateChallengeConditionType(body) {
        this.initHttpHeader();
        return this.httpClient.post(
            this._configService.getAPILink('challengeConditionTypeInsertUpdate').toString(),
            body, 
            this.httpOptions);
    }

    // promocode 
    getPromocode() {
        this.initHttpHeader();
        return this.httpClient.get(
            this._configService.getAPILink('getPromocode').toString(),
            this.httpOptions);
    }

    // get user package
    getUserOfPackage() {
        this.initHttpHeader();
        return this.httpClient.get(
            this._configService.getAPILink('userofpackage').toString(),
            this.httpOptions);
    }
}
