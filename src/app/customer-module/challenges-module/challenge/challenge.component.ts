import { Component, OnInit, OnChanges } from '@angular/core';
import { ChallengeService } from './challenge.service';
import { ResponseData, ChallengeDetailList, ChallengeDetailModel } from './challenge.model';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-challenge',
  styleUrls: ['./challenge.component.scss'],
  templateUrl: './challenge.component.html',
})
export class ChallengeComponent implements OnInit, OnChanges {

  listChallenge: ChallengeDetailModel[] = [];

  constructor(
    private _challengeService: ChallengeService,
    private router: Router
  ) { }

  ngOnInit() {
    this.onChallenge();
  }

  ngOnChanges() {
  }

  onChallenge() {
    this._challengeService.getChallenge()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<ChallengeDetailList>;
            this.listChallenge = response.data.challengeDetailList;
            console.log('Call API Get Challenge', this.listChallenge);
          } catch (error) {
            console.log("errors", error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }

  onAddChallenge() {
    this.router.navigateByUrl('customer/challenges/challengeInsertUpdate');
  };

  onEditChallenge(id){
    this.router.navigate(['customer/challenges/challengeInsertUpdate'], { queryParams: {id : id} });
  }
}

