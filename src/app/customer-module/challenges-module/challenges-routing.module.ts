import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChallengesComponent } from './challenges.component';
import { ChallengeComponent } from './challenge/challenge.component';
import { UpdateChallengeComponent } from './challenge/challengeInsertUpdate/challengeInsertUpdate.component';
import { ChallengeConditionTypeComponent } from './challenge-condition-type/challenge-condition-type.component';
import { UpdateChallengeConditionComponent } from './challenge-condition-type/challenge-condition-type-insert-update/challenge-condition-type-insert-update.component';

export const challengesRoutedComponents = [
  // Root 
  ChallengesComponent, // challenge module component
  ChallengeComponent,
  UpdateChallengeComponent,
  ChallengeConditionTypeComponent,
  UpdateChallengeConditionComponent,
];


const routes: Routes = [{
  path: '',
  component: ChallengesComponent,
  children: [
    {
      path: 'getChallenge',
      component: ChallengeComponent,
    },
    {
      path: 'challengeInsertUpdate',
      component: UpdateChallengeComponent,
    },
    {
      path: 'getChallengeConditionType',
      component: ChallengeConditionTypeComponent,
    },
    {
      path: 'challengeConditionTypeInsertUpdate',
      component: UpdateChallengeConditionComponent,
    },
    {
      path: '',
      redirectTo: 'getChallenge',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ChallengesRoutingModule {

}

