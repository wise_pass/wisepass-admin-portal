import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-grid-a-outline',
    children: [
      {
        title: 'Dashboard',
        link: '/customer/dashboard/dashboard'
      },
    ],
    home: true,
  },
  {
    title: 'Welcome',
    icon: 'nb-compose',
    children: [
      {
        title: 'Information',
        link: '/customer/welcome/information'
      },
      {
        title: 'Admin Permissions',
        link: '/customer/welcome/adminpermissions'
      },
      {
        title: 'Update Facebook',
        link: '/customer/welcome/updatefacebook'
      },
      {
        title: 'Push Notification',
        link: '/customer/welcome/pushnotification'
      },
      {
        title: 'Vendor Payment',
        link: '/customer/welcome/getvendorpayment'
      },
      {
        title: 'Activation',
        link: '/customer/welcome/event'
      },
      {
        title: 'Brand Product',
        link: '/customer/welcome/getbrandproduct'
      },
    ],
  },
  {
    title: 'Challenges',
    icon: 'nb-sunny-circled',
    children: [
      {
        title: 'Challenge',
        link: '/customer/challenges/getChallenge'
      },
      {
        title: 'Challenge Condition Type',
        link: '/customer/challenges/getChallengeConditionType'
      },
    ],
    home: true,
  },
  {
    title: 'Reports',
    icon: 'nb-roller-shades',
    children: [
      {
        title: 'Category Consumption',
        link: '/customer/reports/category'
      },
      {
        title: 'PASS Activity',
        link: '/customer/reports/passactivity'
      },
      {
        title: 'Supplier Consumption',
        link: '/customer/reports/supplier'
      },
      {
        title: 'Promocode Consumption',
        link: '/customer/reports/promocodeConsumptionReport'
      },
      {
        title: 'Reconciliation By Period',
        link: '/customer/reports/reconciliationbyperiod'
      },
      {
        title: 'Management Venues',
        link: '/customer/reports/managementvenues'
      },
      {
        title: 'Management Cities',
        link: '/customer/reports/managementcities'
      },
      {
        title: 'Bank Venues',
        link: '/customer/reports/bankvenues'
      },
      {
        title: 'Appasflyer ',
        link: '/customer/reports/appasflyer'
      },
    ],
    home: true,
  }
];
