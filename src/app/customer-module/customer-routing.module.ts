import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { CustomerComponent } from './customer.component';

const routes: Routes = [{
  path: '',
  component: CustomerComponent,
  children: [
    {
      path: 'welcome',
      loadChildren: './welcome-module/welcome.module#WelcomeModule',
    },
    {
      path: 'challenges',
      loadChildren: './challenges-module/challenges.module#ChallengesModule',
    },
    {
      path: 'reports',
      loadChildren: './reports-module/reports.module#ReportsModule',
    },
    {
      path: 'dashboard',
      loadChildren: './dashboard-module/dashboard.module#DashboardModule',
    },
    {
      path: 'forms',
      loadChildren: './forms/forms.module#FormsModule',
    },
    {
      path: 'extra-components',
      loadChildren: './extra-components/extra-components.module#ExtraComponentsModule',
    },
    {
      path: 'miscellaneous',
      loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
    }, {
      path: '',
      redirectTo: 'dashboard/dashboard',
      pathMatch: 'full',
    }, {
      path: '**',
      component: NotFoundComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerRoutingModule {

}
