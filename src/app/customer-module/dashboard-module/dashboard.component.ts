import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { Router } from '../../../../node_modules/@angular/router';
import { ResponseData, DashboardModel, DashboardDetailModel } from './dashboard.model';
import { AppasflyerInstallsModel } from '../reports-module/report/Appasflyer/appasflyer.model';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

  loading: boolean;
  htmlStr: string;

  activeLocationHCMCount: number = 0;
  activeLocationHNCount: number = 0;
  totalActiveLocation: number = 0;
  membersUseStarterPacksCount: number = 0;
  activeLocationHCMPercent: number = 0;
  activeLocationHNPercent: number = 0;
  totalInstalls: number = 0;

  dashboardDetailList: DashboardDetailModel[];


  constructor(
    private _dashboardService: DashboardService,
    private _router: Router,
  ) { }


  ngOnInit() {
    this.onCallAPIActiveLocation();
  }

  onCallAPIActiveLocation() {
    this.loading = true;
    this._dashboardService.getDashboard()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<DashboardModel>;
            this.dashboardDetailList = response.data.dashboardDetailList;
            // console.log('Call API Dashboard', this.dashboardDetailList);
            if (this.dashboardDetailList && this.dashboardDetailList.length > 0) {
              this.dashboardDetailList.forEach(item => {
                this.activeLocationHCMCount = item.activeLocationHoChiMinhCount;
                this.activeLocationHCMPercent = Math.round((this.activeLocationHCMCount / 40) * 100);
                this.activeLocationHNCount = item.activeLocationHaNoiCount;
                this.activeLocationHNPercent = Math.round((this.activeLocationHNCount / 6) * 100); 
                this.membersUseStarterPacksCount = item.membersUseStarterPacksCount;
                this.totalActiveLocation = item.totalActiveLocation;
              });
            }
          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );

    this.loading = true;
    this._dashboardService.getAppasflyer()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<AppasflyerInstallsModel>;
            this.totalInstalls = response.data.totalInstalls;
            // console.log("TCL: DashboardComponent -> onCallAPIActiveLocation -> this.totalInstalls", this.totalInstalls)

          } catch (error) {
            console.log('errors', error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }
}
