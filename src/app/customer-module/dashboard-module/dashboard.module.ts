import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '../../../../node_modules/@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbDatepickerModule, NbCalendarModule } from '../../../../node_modules/@nebular/theme';
import { DashboardRoutedComponents, DashboardRoutingModule } from './dashboard.-routing.module';
import { DashboardService } from './dashboard.service';

@NgModule({
  imports: [
    ThemeModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbDatepickerModule,
    NbCalendarModule,
  ],
  declarations: [
    ...DashboardRoutedComponents,
  ],
  providers: [
    DashboardService,
  ]
})
export class DashboardModule { }
