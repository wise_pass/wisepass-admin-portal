
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface DashboardModel {
    dashboardDetailList: Array<DashboardDetailModel>
}

export interface DashboardDetailModel {
    totalActiveLocation: number;
    activeLocationHaNoiCount: number;
    activeLocationHoChiMinhCount: number;
    membersUseStarterPacksCount: number;
}

export interface TotalInstallsModel {
    totalInstalls: number;
}