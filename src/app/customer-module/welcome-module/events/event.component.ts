import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';
import { EventService } from './event.service';
import { ReponseDataList, EventModel, EventsList } from './event.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-event',
  styleUrls: ['./event.component.scss'],
  templateUrl: './event.component.html',
})
export class EventComponent implements OnInit {
  loading: boolean;
  events: EventsList[] = [];

  settings = {
    columns: {
      numericalOrder: {
        title: 'STT',
        width: '5%',
        editable: false,
      },
      name: {
        title: 'Name',
        filter: true,
        valuePrepareFunction: (data) => {
          return data.text;
        },
        filterFunction(cell?: any, search?: string): boolean {
          const match = cell.text.toLowerCase().indexOf(search) > -1;
          if (match || search === '') {
            return true;
          } else {
            return false;
          }
        },
      },
      venue: {
        title: 'Venue',
        filter: true,
        valuePrepareFunction: (data) => {
          return data.name;
        },
        filterFunction(cell?: any, search?: string): boolean {
          const match = cell.name.toLowerCase().indexOf(search) > -1;
          if (match || search === '') {
            return true;
          } else {
            return false;
          }
        },
      },
      'venue.address': {
        title: 'City',
        valuePrepareFunction: (cell, row) => {
          return row.venue.address.city
        },
      },
      end: {
        title: 'Date', sortDirection: 'desc',
        valuePrepareFunction: (data) => {
          var raw = new Date(data.local);
          var formatted = this.datePipe.transform(raw, 'dd MMM yyyy');
          return formatted;
        },
      },
      sales_data_with_null: {
        title: 'Registrations',
        valuePrepareFunction: (data) => {
          return data.total_ticket_sales;
        },
      },
      status: {
        title: 'Status',
      },
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
    },
    pager: {
      display: true,
      perPage: 20
    },

  };

  constructor(
    private _eventService: EventService,
    private _router: Router,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.onAPIEvent();
  }

  onAPIEvent() {
    this.loading = true;
    // getvenue
    this._eventService.getEvents()
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<EventModel>;
            this.events = response.data.events;
            // console.log('Call API Get Event', this.events);

          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }
}

