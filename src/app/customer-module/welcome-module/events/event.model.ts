
export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: T;
}

// Vendor
export interface EventModel {
    events: Array<EventsList>
}

export interface EventsList {
    name: Array<Name>;
    start: Array<Start>;
    end: Array<End>;
    sales_data_with_null: Array<SalesDataWithNull>;
    venue: Array<Venue>;
    changed: string;
    created: string;
    status: string;
}
export interface Name {
    text: string;
}
export interface Start {
    timezone: string;
    local: string;
}
export interface End {
    timezone: string;
    local: string;
}
export interface SalesDataWithNull{
    total_ticket_sales: number;
}
export interface Venue {
    name: string;
}