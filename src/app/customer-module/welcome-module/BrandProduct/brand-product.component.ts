import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';
import { BrandProductService } from './brand-product.service';
import { ReponseDataList, BrandProductDetailModel, GetBrandProductDetailModel, SelectGraphFacebookModel, SelectGraphFacebookDetailModel } from './brand-product.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-brand-product',
  styleUrls: ['./brand-product.component.scss'],
  templateUrl: './brand-product.component.html',
})
export class BrandProductComponent implements OnInit {

  loading: boolean;
  listBrandProductDetail: GetBrandProductDetailModel[];
  listGraphFacebookDetail: SelectGraphFacebookDetailModel[];

  constructor(
    private _brandProductService: BrandProductService,
    private _router: Router,
    private datePipe: DatePipe
  ) { }

  settingsProduct = {
    columns: {
      createDate: {
        title: 'Date',
        editable: false,
        valuePrepareFunction: (date) => { 
          var raw = new Date(date);
          var formatted = this.datePipe.transform(raw, 'dd MMM yyyy');
          return formatted; 
        }
      },
      brand: {
        title: 'Brand',
        editable: false,
      },
      numberSearchResult: {
        title: 'Number Search Result',
        editable: false,
        valuePrepareFunction: (value) => value === 'Number' ? value : Intl.NumberFormat('vi-VN').format(value)
      },
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    pager: {
      display: true,
      perPage: 20
    },
  };

  settingsGraph = {
    columns: {
      createdDate: {
        title: 'Date',
        editable: false,
        valuePrepareFunction: (date) => { 
          var raw = new Date(date);
          var formatted = this.datePipe.transform(raw, 'dd MMM yyyy');
          return formatted; 
        }
      },
      userName: {
        title: 'User Name',
        editable: false,
      },
      fanCount: {
        title: 'Fan count',
        editable: false,
        valuePrepareFunction: (value) => value === 'Number' ? value : Intl.NumberFormat('vi-VN').format(value)
      },
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    pager: {
      display: true,
      perPage: 20
    },
  };

  ngOnInit() {
    this.callAPIBrandProduct();
    this.callAPIGraphFacebook();
  }

  callAPIBrandProduct() {
    this.loading = true;
    this._brandProductService.getBrandProduct()
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<BrandProductDetailModel>;
            this.listBrandProductDetail = response.data.getBrandProductDetailList;
            // console.log('Call API Brand Product', this.listBrandProductDetail);
          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          this.loading = false;
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }

  callAPIGraphFacebook() {
    this.loading = true;
    this._brandProductService.getGraphFacebook()
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<SelectGraphFacebookModel>;
            this.listGraphFacebookDetail = response.data.selectGraphFacebookDetailList;
            console.log('Call API listGraphFacebookDetail', this.listGraphFacebookDetail);
            
          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          this.loading = false;
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }

  onBrandProduct(){
    this._router.navigateByUrl('/customer/welcome/insertbrandproduct');
  }
}

