import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../../node_modules/@angular/router';
import { BrandProductService } from '../brand-product.service';
import { ReponseDataList, BrandProductInsertModel } from '../brand-product.model';

@Component({
  selector: 'ngx-insert-brand-product',
  styleUrls: ['./insert-brand-product.component.scss'],
  templateUrl: './insert-brand-product.component.html',
})
export class InsertBrandProductComponent implements OnInit {

  body: any = {};
  brand: string;
  keySearch: string;

  constructor(
    private _brandProductService: BrandProductService,
    private _router: Router,
  ) { }

  ngOnInit() {

  }

  onCreateBrandSubmit() {
    this.callAPIInsertBrandProduct();
  }

  callAPIInsertBrandProduct() {
    this.body = {
      brand: this.brand,
      keySearch: this.keySearch,
    }
    this._brandProductService.insertBrandProduct(this.body)
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<BrandProductInsertModel>;
            // console.log('Call API Brand Product', response);
            if (response.statusCode == 200) {
              this._router.navigateByUrl('/customer/welcome/getbrandproduct');
            }
          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }
}

