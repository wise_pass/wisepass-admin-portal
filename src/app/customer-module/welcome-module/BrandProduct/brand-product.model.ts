
export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: T;
}
// get brand product detail
export interface BrandProductDetailModel {
    getBrandProductDetailList: Array<GetBrandProductDetailModel>
}
export interface GetBrandProductDetailModel {
    id: string;
    createDate: string;
    numberSearchResult: string;
    brand: string;
}

// insert brand product
export interface BrandProductInsertModel {
    dataBrandProductInsertList: Array<DataBrandProductInsertModel>
}
export interface DataBrandProductInsertModel {

}

// GraphFacebook

export interface SelectGraphFacebookModel {
    selectGraphFacebookDetailList: Array<SelectGraphFacebookDetailModel>
}
export interface SelectGraphFacebookDetailModel {
    id: string;
    username: string;
    fan_count: number;
}