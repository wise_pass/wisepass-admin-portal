import { Component, OnInit } from '@angular/core';
import { VendorPaymentService } from './vendorPayment.service';
import { ReponseDataList, VendorPaymentModel, VendorPaymentByCityLocationModel, VendorPaymentByVenueList, DataModel } from './vendorPayment.model';
import { Router } from '../../../../../node_modules/@angular/router';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'ngx-vendor-payment',
  styleUrls: ['./vendorPayment.component.scss'],
  templateUrl: './vendorPayment.component.html',
})
export class VendorPaymentComponent implements OnInit {

  firstDay: string;
  lastDay: string;
  internationalZoneId: string;
  selectedItemCity: string = "";
  month: string;
  htmlStr: string;
  totalAmount: number = 0;
  businessVenueId: string = "";
  total: number = 0;
  totalPayment: number = 0;
  totalUnpaid: number = 0;
  isValid: boolean = true;
  selectName = "total";
  currency = "";
  vendorPaymentByVenueList: VendorPaymentByVenueList[];
  loading: boolean;
  userId: string;
  body: any = {};
  statusUnPaid: number = 0;
  city: VendorPaymentByCityLocationModel[];

  settings = {
    columns: {
      businessName: {
        title: 'Business',
        editable: false,
      },
      businessVenueName: {
        title: 'Venue',
        editable: false,
      },
      totalAmount: {
        title: 'Amount',
        valuePrepareFunction: (value) => value === 'Total' ? value : Intl.NumberFormat('vi-VN').format(value)
      },
      
      status: {
        title: 'Status', sortDirection: 'desc',
        width: '6%',
        editor: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: 
            [
              { value: 'YES', title: 'YES' },
              { value: 'NO', title: 'NO' },
            ],
          },
        }
      },
      period: {
        title: 'Month Year',
        editable: false,
      },
      cityLocationName: {
        title: 'City',
        editable: false,
      }
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: true,
      delete: false,
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmSave: true,
    },
    pager: {
      display: true,
      perPage: 20
    },
  };

  constructor(
    private _vendorPaymentService: VendorPaymentService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.onAPIVendorPayment();
  }

  onAPIVendorPayment() {
    this.loading = true;
    // getvenue
    this._vendorPaymentService.getVendorPayment()
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<VendorPaymentModel>;
            this.vendorPaymentByVenueList = response.data.vendorPaymentByVenueList;
            // console.log('Call API Get User Vendor Payment', this.vendorPaymentByVenueList);
            let totalPrice = 0;
            let totalPayment = 0;
            let countStatus = 0;

            if (this.vendorPaymentByVenueList && this.vendorPaymentByVenueList.length > 0) {
              this.vendorPaymentByVenueList.forEach(element => {
                this.currency = element.currency;
                this.total = totalPrice += element.totalAmount;
                if (element.status == 'YES') {
                  this.totalPayment = totalPayment += element.totalAmount;
                } else if (element.status == 'NO') {
                  this.statusUnPaid = countStatus++;
                }
                this.totalUnpaid = this.total - this.totalPayment;
              });
            }
          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }

  onSaveConfirm(event) {
    const infoJwt = jwt_decode(localStorage.getItem('token') ? localStorage.getItem('token') : '');
    if (infoJwt) {
      this.userId = infoJwt.UserId;
    }
    if (window.confirm('Are you sure you want to save?')) {
      const business = event.newData;
      if (business.businessVenueId != null) {
        this.body = {
          businessVenueId: business.businessVenueId,
          period: business.period,
          totalAmount: business.totalAmount,
          status: business.status,
          userId: this.userId,
        }
      } else if (business.businessId != null) {
        this.body = {
          businessId: business.businessId,
          period: business.period,
          totalAmount: business.totalAmount,
          status: business.status,
          userId: this.userId,
        }
      }
      this.loading = true;
      this._vendorPaymentService.updateVendorPayment(this.body)
        .subscribe(
          data => {
            try {
              const response = data as ReponseDataList<DataModel>;
              // console.log('call API response',response)
              if (response.statusCode == 200) {
                this._router.navigateByUrl('customer/welcome/getvendorpayment');
              }
            } catch (error) {
              console.log("errors", error);
              this._router.navigate(['/logout']);
            }
          }, error => {
            console.log('oops', error)
          },
          () => {
            // 'onCompleted' callback.
            // No errors, route to new page here
            this.loading = false;
          },
      );
    } else {
      event.confirm.reject();
      console.log("up", event);
    }
  }
}

