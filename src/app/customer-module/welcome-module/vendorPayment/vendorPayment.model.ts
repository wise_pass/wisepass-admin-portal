
export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: T;
}

// Vendor
export interface VendorPaymentModel {
    vendorPaymentByVenueList: Array<VendorPaymentByVenueList>
}

export interface VendorPaymentByVenueList {
    cityLocationId: string;
    cityLocationName: string;
    businessId: string;
    businessName: string;
    businessVenueId: string;
    businessVenueName: string;
    period: string;
    totalAmount: number;
    status: string;
    currency: string;
    bankAccountNumber: string;
    bankName: string;
    companyName: string;
}

// Insert
export interface DataModel {
    message: string;
    statusCode: number;
    data: string;
}

// City
export interface VendorPaymentByCityLocationList {
    vendorPaymentByCityLocationList: Array<VendorPaymentByCityLocationModel>
}

export interface VendorPaymentByCityLocationModel {
    cityLocationId: string;
    cityLocationName: string;
}