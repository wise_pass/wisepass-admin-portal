export interface UpdateFacebookModel {
    message: string;
    statusCode: number;
    data: Data;
}

export interface Data {
    venues : Venues[]
}

export interface Venues{
    id: string;
    name: string;
    image: string;
    backgroundImage : string;
    facebook: string;
}
