import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { ConfigService } from '../../../configuration/configuration';
import * as jwt_decode from 'jwt-decode';
import { Data, Venues } from './update-facebook.model';

@Injectable()
export class UpdateFacebookService {

    constructor(private http: HttpClient, public _configService: ConfigService) { }

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error) {
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            },
        );
    }

    getVenue() {
        this.initHttpHeader();
        return this.http.get(
            this._configService.getAPILink('getVenue').toString(),
            // venue,
            this.httpOptions);
    }

    updatVenue(venue) {
        this.initHttpHeader();
        return this.http.post(
            this._configService.getAPILink('updateVenue').toString(),
            venue,
            this.httpOptions);
    }

}
