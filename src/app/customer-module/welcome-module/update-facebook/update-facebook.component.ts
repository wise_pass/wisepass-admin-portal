import { Component, OnInit } from '@angular/core';
import { UpdateFacebookService } from './update-facebook.service';
import { UpdateFacebookModel, Venues } from './update-facebook.model';

@Component({
  selector: 'ngx-update-facebook',
  styleUrls: ['./update-facebook.component.scss'],
  templateUrl: './update-facebook.component.html',
})
export class UpdateFacebookComponent implements OnInit {
  updateFacebook: UpdateFacebookModel;
  listVenue = [];
  businessVenue: Venues[];
  seachKeyWord : string;

  constructor(
    private _updateFacebookService: UpdateFacebookService,
  ) { }

  onGetInformation() {
    this._updateFacebookService.getVenue()
      .subscribe(
        data => {
          try {
            this.updateFacebook = data as UpdateFacebookModel;
            if (this.updateFacebook.data && this.updateFacebook.statusCode === 200) {
              this.listVenue = this.updateFacebook.data.venues;
            }
          } catch {

          }
        },
        err => { },
        () => { },
    );
  }

  ngOnInit() {
    this.onGetInformation();
  }

  onSave(bvId) {
    this.businessVenue = this.listVenue.find((bvItem) => bvItem.Id === bvId);

    this._updateFacebookService.updatVenue(this.businessVenue)
      .subscribe(
        data => {
          try {

          } catch {

          }
        },
        err => { },
        () => { },
    );

  }

}
