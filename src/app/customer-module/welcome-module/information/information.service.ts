import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { ConfigService } from '../../../configuration/configuration';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class InformationService {

    constructor(private http: HttpClient, public _configService: ConfigService) {}

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error) {
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            },
        );
    }

    getAllCompanyinformation() {
        this.initHttpHeader();

        return this.http.get(
            this._configService.getAPILink('getallcompanies').toString(),
            this.httpOptions);

        // return this.http.post(
        //     this._configService.getAPILink('validateemail').toString(),
        //     item);
    }

    getvenueinformation() {
        this.initHttpHeader();

        return this.http.get(
            this._configService.getAPILink('getvenueinformation').toString(),
            this.httpOptions);

        // return this.http.post(
        //     this._configService.getAPILink('validateemail').toString(),
        //     item);
    }

    saveVenueInformation(item) {
        this.initHttpHeader();

        return this.http.post(
            this._configService.getAPILink('savevenueinformation').toString(),
            item,
            this.httpOptions);

        // return this.http.post(
        //     this._configService.getAPILink('validateemail').toString(),
        //     item);
    }

    validateOtp(item) {
        this.initHttpHeader();
        return this.http.post(
            this._configService.getAPILink('validateotp').toString(),
            item);
    }

    refresh(item) {
        this.initHttpHeader();
        // return this.http.post(
        //     this._configService.getAPILink('auth_refresh').toString(),
        //     item,
        //     this.httpOptions);
        return this.http.post(
            this._configService.getAPILink('auth_refresh').toString(),
            item);
    }
}
