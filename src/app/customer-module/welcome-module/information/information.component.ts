import { Component, OnInit } from '@angular/core';
import { InformationService } from './information.service';
import { CheckTokenService } from '../../../configuration/check-token-service';
import { Router } from '@angular/router';

import 'style-loader!angular2-toaster/toaster.css';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { FormGroup, FormBuilder } from '@angular/forms';



@Component({
  selector: 'ngx-welcome-validate-company',
  styleUrls: ['./information.component.scss'],
  templateUrl: './information.component.html',
})
export class WelcomeInformationComponent implements OnInit {
  constructor(
    private _informationService: InformationService,
    private _checkTokenService: CheckTokenService,
    private toastrService: NbToastrService,
    private _router: Router,
    private fb: FormBuilder
  ) { }

  company : Company;
  venue : Venue;
  bank : Bank;
  dateOpening : DateOpening[] = [];
  dateOpeningOneday : DateOpening;

  listInfomation: InformationModel[];

  // Step by Step
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;

  private showToast(type: NbToastStatus, title: string, body: string) {
    const _toastConfig = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };

    this.toastrService.show(
      body,
      title,
      _toastConfig);
    }
    
  onGetInformation() {
    this._informationService.getAllCompanyinformation()
    .subscribe(
      data => {
        try {
          var item = data as VenueInformationModel;
          if (item.data && item.statusCode === 200) { 
            this.listInfomation = item.data;

            this.listInfomation.forEach(item => {
              item.venue.dateOpeningObject = JSON.parse(item.venue.dateOpening);
              item.venue.dateOpeningOnedayObject = JSON.parse(item.venue.dateOpeningOneday);
            });
          }
          
          // console.log(this.listInfomation);
        } catch {

        }
      },
      err => { },
      () => { },
    );

    // this._informationService.getvenueinformation()
    // .subscribe(
    //   data => {
    //     try {
    //       var item = data as GetVenueInformationModel;

    //       if (item.data && item.statusCode === 200) {
    //         if (item.data.company) {
    //           var company = item.data.company;

    //           this.company = {
    //             address : company.address,
    //             countryId : company.countryId,
    //             fbId : company.fbId,
    //             fbPageUrl : company.fbPageUrl,
    //             name : company.name,
    //             phoneNo : company.phoneNo,
    //             tax : company.tax,
    //             webUrl : company.webUrl,
    //           };
    //         }

    //         if (item.data.venue) {
    //           var venue = item.data.venue;

    //           this.venue = {
    //             id: venue.id,
    //             dateOpening: venue.dateOpening,
    //             dateOpeningOneday: venue.dateOpeningOneday,
    //             description: venue.description,
    //             fbId: venue.fbId,
    //             logoUrl: venue.logoUrl,
    //             name: venue.name,
    //             address: venue.address ? venue.address : "",
    //             phoneNo: venue.phoneNo ? venue.phoneNo : "",
    //             typeDateOpening: venue.typeDateOpening ? venue.typeDateOpening : 0
    //           };

    //           if (this.venue.dateOpening){
    //             this.dateOpening = JSON.parse(this.venue.dateOpening);
    //           }

    //           if (this.venue.dateOpeningOneday) {
    //             this.dateOpeningOneday = JSON.parse(this.venue.dateOpeningOneday);

    //             // this.dateOpeningOneday = {
    //             //   dayOfWeek: dateTempo.dayOfWeek,
    //             //   timeOpen: dateTempo.timeOpen,
    //             //   timeClosed: dateTempo.timeClosed,
    //             //   on: dateTempo.on
    //             // }
    //           }
    //         }

    //         if (item.data.bank)
    //         {
    //           var bank = item.data.bank;

    //           this.bank = {
    //             no: bank.no,
    //             name: bank.name,
    //             holder: bank.holder,
    //             branch: bank.branch,
    //             swiftCode: bank.swiftCode
    //           }
    //         }
    //       } else {
    //         this._router.navigate(['/logout']);
    //       }
    //     } catch {
    //       this._router.navigate(['/logout']);
    //     }
    //   },
    //   err => { },
    //   () => { },
    // );
  }

  onSave() {
    var item = {
      company: this.company,
      venue: this.venue,
      bank: this.bank
    }

    if (item.venue.typeDateOpening.toString() == "true" ||
    item.venue.typeDateOpening.toString() == "1")
    {
      item.venue.typeDateOpening = 1;
    } else {
      item.venue.typeDateOpening = 0;
    }

    item.venue.dateOpening = JSON.stringify(this.dateOpening);
    item.venue.dateOpeningOneday = JSON.stringify(this.dateOpeningOneday);
    
    this._informationService.saveVenueInformation(item)
    .subscribe(
      data => {
        var item = data as GetVenueInformationModel;
        let textInformation = "Save Information ";

        if (!item.data || item.statusCode != 200)
        {
          this.showToast(NbToastStatus.DANGER, "Failed", textInformation + item.message);
        }

        if (item.statusCode === 701) {
          this._router.navigate(['/logout']);
        } else {
          this.showToast(NbToastStatus.SUCCESS, "Succeed", textInformation + item.message);
        }
      },
      err => { },
      () => { },
    );
  }

  ngOnInit() {
    this.company = new Company();
    this.venue = new Venue();
    this.bank = new Bank();
    this.dateOpening = new Array<DateOpening>();
    this.listInfomation = new Array<InformationModel>();
    if (this._checkTokenService.isCheck())
    {
      this.onGetInformation();
    }

    // Step by Step 
    this.firstForm = this.fb.group({
      // firstCtrl: ['', Validators.required]
    });

    this.secondForm = this.fb.group({
      // secondCtrl: ['', Validators.required],
    });

    this.thirdForm = this.fb.group({
      // thirdCtrl: ['', Validators.required],
    });
  }


  // Step by Step 
  onFirstSubmit() {
    this.firstForm.markAsDirty();
  }

  onSecondSubmit() {
    this.secondForm.markAsDirty();
  }

  onThirdSubmit() {
    this.thirdForm.markAsDirty();
  }
}

class DateOpening {
  dayOfWeek: string;
  timeOpen: string;
  timeClosed: string;
  on: number;
}

class Company {
  address: string;
  countryId: string;
  fbId: string;
  fbPageUrl: string;
  name: string;
  phoneNo: string;
  tax: string;
  webUrl: string;
  createStatus: number;
  createdPercent: number;
}

class Bank {
  no: string;
  holder: string;
  name: string;
  branch: string;
  swiftCode: string;
}

class Member {
  email: string;
}


class Venue {
  id: string;
  dateOpening: string;
  dateOpeningOneday: string;
  description: string;
  fbId: string;
  logoUrl: string;
  address: string;
  phoneNo: string;
  name: string;
  typeDateOpening: number;

  dateOpeningObject : DateOpening[] = [];
  dateOpeningOnedayObject : DateOpening;
}

class GetVenueInformationModel {
  data: {
    company: Company,
    venue: Venue,
    bank: Bank
  }
  statusCode: number;
  message: string
}

class VenueInformationModel {
  data: InformationModel[];
  statusCode: number;
  message: string
}

class InformationModel {
  company: Company;
  venue: Venue;
  bank: Bank;
  member: Member;
}
