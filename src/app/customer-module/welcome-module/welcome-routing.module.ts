import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome.component';
import { WelcomeInformationComponent } from './information/information.component';

import { FormInputsComponent } from './form-inputs/form-inputs.component';
import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { AdminPermissionsComponent } from './adminPermissions/adminPermissions.component';
import { UpdateFacebookComponent } from './update-facebook/update-facebook.component';
import { PushNotificationComponent } from './pushNotification/pushNotification.component';
import { VendorPaymentComponent } from './vendorPayment/vendorPayment.component';
import { EventComponent } from './events/event.component';
import { BrandProductComponent } from './BrandProduct/brand-product.component';
import { InsertBrandProductComponent } from './BrandProduct/InsertBrandProduct/insert-brand-product.component';

const routes: Routes = [{
  path: '',
  component: WelcomeComponent,
  children: [
    {
      path: 'information',
      component: WelcomeInformationComponent,
    },
    {
      path: 'adminpermissions',
      component: AdminPermissionsComponent,
    },
    {
      path: 'updatefacebook',
      component: UpdateFacebookComponent,
    },
    {
      path: 'pushnotification',
      component: PushNotificationComponent,
    },
    {
      path: 'getvendorpayment',
      component: VendorPaymentComponent,
    },
    {
      path: 'event',
      component: EventComponent,
    },
    {
      path: 'getbrandproduct',
      component: BrandProductComponent,
    },
    {
      path: 'insertbrandproduct',
      component: InsertBrandProductComponent,
    },
    {
      path: 'inputs',
      component: FormInputsComponent,
    },
    {
      path: 'layouts',
      component: FormLayoutsComponent,
    },
    {
      path: 'layouts',
      component: FormLayoutsComponent,
    },
    {
      path: 'buttons',
      component: ButtonsComponent,
    },
    {
      path: 'datepicker',
      component: DatepickerComponent,
    },
     {
      path: '',
      redirectTo: 'information',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class WelcomeRoutingModule {

}

export const routedComponents = [
  // Root 
  WelcomeComponent,

  // Child
  WelcomeInformationComponent,
  AdminPermissionsComponent,
  UpdateFacebookComponent,
  PushNotificationComponent,
  VendorPaymentComponent,
  EventComponent,
  BrandProductComponent,
  InsertBrandProductComponent,

  FormInputsComponent,
  FormLayoutsComponent,
  DatepickerComponent
];
