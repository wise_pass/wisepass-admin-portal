import { Component, OnInit, OnChanges } from '@angular/core';
import { AdminPermissionModel, ReponseDataList } from './adminPermissions.model';
import { AdminPermissionsService } from './adminPermissions.service';
import { NbToastrService } from '../../../../../node_modules/@nebular/theme/components/toastr/toastr.service';

@Component({
  selector: 'ngx-admin-permissions',
  styleUrls: ['./adminPermissions.component.scss'],
  templateUrl: './adminPermissions.component.html',
})
export class AdminPermissionsComponent implements OnInit, OnChanges {

  id: string = "";
  name: string = "";
  email: string = "";
  seachKeyWord: string = "";
  selectedDropDownItem: string = "";
  inputEmail: string = "";
  groupBusinessFilter: AdminPermissionModel[] = [];
  adminPermissions: AdminPermissionModel[] = [];
  constructor(
    private _adminPermissionsService: AdminPermissionsService,
    private toastrService: NbToastrService,
  ) { }

  ngOnInit() {
    this._adminPermissionsService.getAdminPermissionReport()
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<AdminPermissionModel>;
            this.groupBusinessFilter = response.data;
            this.keyUpSearch();
            console.log('Call API Get User Admin permission', this.groupBusinessFilter);

          } catch (error) {
            console.log("errors", error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );


  }

  ngOnChanges() {
  }

  keyUpSearch() {
    // console.log(this.seachKeyWord);
    if (this.groupBusinessFilter) {
      this.adminPermissions = this.groupBusinessFilter.filter((item) => {
        return (item.businessName || '').toLowerCase().includes(this.seachKeyWord.toLowerCase());
      });
    }
  }

  onClickAddInput(adminPermission: AdminPermissionModel) {
    adminPermission.memberEmails.push(
      {
        id: null,
        memberEmail: ""
      }
    );
  }

  onClickDeleteInput(adminPermission: AdminPermissionModel, index : number){
    adminPermission.memberEmails.splice(index); 
    if (index != 0){
      adminPermission.memberEmails.splice(index); 
    }else{
      adminPermission.memberEmails[index].memberEmail = "";
    }
  }

  onClickSubmit(adminPermission: AdminPermissionModel, position, status) {
    this._adminPermissionsService.updatMemberByBusiness(adminPermission)
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<AdminPermissionModel>;
            console.log('Call API Get User Update / Delete / Insert', response);
            if (response.statusCode == 200) {
              this.toastrService.show(
                status || 'Success',
                adminPermission.businessName,
                { position, status });
            }
          } catch (error) {
            console.log("errors", error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }
}

