
export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: Array<T>;
}


export interface AdminPermissionModel {
    businessId: string;
    businessName: string;
    permissionType: number;
    memberEmails: Email[];
}

export interface Email {
    id: string;
    memberEmail: string;
}