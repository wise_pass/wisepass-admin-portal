import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { WelcomeRoutingModule, routedComponents } from './welcome-routing.module';
import { ButtonsModule } from './buttons/buttons.module';

// import { NgDatepickerModule } from 'ng2-datepicker';
// Services
import { CheckTokenService } from '../../configuration/check-token-service';
import { InformationService } from './information/information.service';
import { AdminPermissionsService } from './adminPermissions/adminPermissions.service';
import { UpdateFacebookService } from './update-facebook/update-facebook.service';
import { PushNotificationService } from './pushNotification/pushNotification.service';
import { CommonModule } from '../../../../node_modules/@angular/common';
// import { DpDatePickerModule} from 'ng2-date-picker';
import { NbRadioModule, NbSpinnerModule, NbToastrModule } from '../../../../node_modules/@nebular/theme';
import { VendorPaymentService } from './vendorPayment/vendorPayment.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { EventService } from './events/event.service';
import { BrandProductService } from './BrandProduct/brand-product.service';

@NgModule({
  imports: [
    ThemeModule,
    WelcomeRoutingModule,
    ButtonsModule,
    CommonModule,
    // DpDatePickerModule,
    NbRadioModule,
    NbSpinnerModule,
    Ng2SmartTableModule,
    NbToastrModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    CheckTokenService,
    InformationService,
    AdminPermissionsService,
    UpdateFacebookService,
    PushNotificationService,
    VendorPaymentService,
    EventService,
    BrandProductService
  ]
})
export class WelcomeModule { }
