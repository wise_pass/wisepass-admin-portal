import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-logout',
  templateUrl: './logout.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class CustomizeLogoutComponent implements OnInit {
  constructor(
    public router: Router,
  ) { }

  ngOnInit() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
